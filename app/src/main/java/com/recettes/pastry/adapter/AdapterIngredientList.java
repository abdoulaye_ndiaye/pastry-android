package com.recettes.pastry.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.recettes.pastry.R;
import com.recettes.pastry.activity.ActivityShopList;
import com.recettes.pastry.model.Ingredien;
import com.recettes.pastry.model.News;
import com.recettes.pastry.model.Shop;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.recettes.pastry.activity.BaseActivity.getUid;

public class AdapterIngredientList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<String> items = new ArrayList<>();

    private Context ctx;
    private News news;
    private OnItemClickListener mOnItemClickListener;
    DatabaseReference shopRef;

    public interface OnItemClickListener {
        void onItemClick(View view, News obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterIngredientList(Context context, List<String> items, News recipe) {
        this.items = items;
        ctx = context;
        news = recipe;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView instruction;
        public ImageView add;
        public ImageView del;

        public ViewHolder(View v) {
            super(v);
            instruction = v.findViewById(R.id.id_instruction);
            add = v.findViewById(R.id.id_add_item);
            del = v.findViewById(R.id.id_remove_item);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_ingredients, parent, false);
        return new ViewHolder(v);
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (getUid()!=null){
            shopRef = FirebaseDatabase.getInstance().getReference("shop").child(getUid());
        }else {
            shopRef = FirebaseDatabase.getInstance().getReference("shop").child("");
        }
        final String ingregien = items.get(position);
//        final String id = UUID.randomUUID().toString();
        final Ingredien ingredien = new Ingredien(ingregien, news.getId(), position);
        final ViewHolder vItem = (ViewHolder) holder;
        vItem.instruction.setText(ingregien);
        shopRef.child(news.getId()).child(position+"").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue()  != null){
                    vItem.del.setVisibility(View.VISIBLE);
                    vItem.add.setVisibility(View.GONE);
                }else {
                    vItem.del.setVisibility(View.GONE);
                    vItem.add.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

        vItem.instruction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                shopRef.child(news.getId()).child(position+"").addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue()  == null){
                            shopRef.child(news.getId()).child(position+"").setValue(ingredien);
                            shopRef.child(news.getId()).child("infos").setValue(news);

                            Snackbar snackbar = Snackbar
                                    .make(view , ingregien + " ajouté à votre liste de course", Snackbar.LENGTH_LONG)
                                    .setAction("Ouvrir list", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent intentShop = new Intent(getApplicationContext(), ActivityShopList.class);
                                            ctx.startActivity(intentShop);
                                        }
                                    });

                            snackbar.setActionTextColor(ctx.getResources().getColor(R.color.colorPrimary));

                            View sbView = snackbar.getView();
                            TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                            textView.setTextColor(ctx.getResources().getColor(R.color.grey_medium));

                            snackbar.show();
                        }else {
                            shopRef.child(news.getId()).child(position+"").removeValue();
                            Snackbar snackbar = Snackbar
                                    .make(view , ingregien + " supprimé de votre liste de course", Snackbar.LENGTH_LONG)
                                    .setAction("Ouvrir la liste", new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            Intent intentShop = new Intent(getApplicationContext(), ActivityShopList.class);
                                            ctx.startActivity(intentShop);
                                        }
                                    });

                            snackbar.setActionTextColor(ctx.getResources().getColor(R.color.colorPrimary));

                            View sbView = snackbar.getView();
                            TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
                            textView.setTextColor(ctx.getResources().getColor(R.color.grey_medium));

                            snackbar.show();
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }
        });

//        vItem.add.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                shopRef.child(news.getId()).child(position+"").setValue(ingredien);
//                shopRef.child(news.getId()).child("infos").setValue(news);
//
//                Snackbar snackbar = Snackbar
//                        .make(view , ingregien + " added to Shopping list", Snackbar.LENGTH_LONG)
//                        .setAction("View list", new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                Intent intentShop = new Intent(getApplicationContext(), ActivityShopList.class);
//                                ctx.startActivity(intentShop);
//                            }
//                        });
//
//                snackbar.setActionTextColor(ctx.getResources().getColor(R.color.colorPrimary));
//
//                View sbView = snackbar.getView();
//                TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
//                textView.setTextColor(ctx.getResources().getColor(R.color.grey_medium));
//
//                snackbar.show();
//            }
//        });
//        vItem.del.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                shopRef.child(news.getId()).child(position+"").removeValue();
//                Snackbar snackbar = Snackbar
//                        .make(view , ingregien + " removed from Shopping list", Snackbar.LENGTH_LONG)
//                        .setAction("View list", new View.OnClickListener() {
//                            @Override
//                            public void onClick(View view) {
//                                Intent intentShop = new Intent(getApplicationContext(), ActivityShopList.class);
//                                ctx.startActivity(intentShop);
//                            }
//                        });
//
//                snackbar.setActionTextColor(ctx.getResources().getColor(R.color.colorPrimary));
//
//                View sbView = snackbar.getView();
//                TextView textView = sbView.findViewById(android.support.design.R.id.snackbar_text);
//                textView.setTextColor(ctx.getResources().getColor(R.color.grey_medium));
//
//                snackbar.show();
//            }
//        });
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    public String getItem(int position) {
        return items.get(position);
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return items.size();
    }

}