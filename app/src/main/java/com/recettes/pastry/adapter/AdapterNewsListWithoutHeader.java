package com.recettes.pastry.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.recettes.pastry.R;
import com.recettes.pastry.model.News;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.List;

public class AdapterNewsListWithoutHeader extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<News> items = new ArrayList<>();
    private List<News> filtered_items = new ArrayList<>();
    private ItemFilter mFilter = new ItemFilter();

    private Context ctx;
    private News header;
    private static final int TYPE_ITEM = 1;
    private static final int NATIVE_AD_VIEW_TYPE = 2;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, News obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public AdapterNewsListWithoutHeader(Context context, News header, List<News> items) {
        this.items = items;
        this.filtered_items = items;
        this.header = header;
        ctx = context;
    }

    public Filter getFilter() {
        return mFilter;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView cuisson;
        public TextView short_content;
        public TextView date;
        public ImageView image;
        public LinearLayout lyt_parent;

        public ViewHolder(View v) {
            super(v);
            title = (TextView) v.findViewById(R.id.title);
            cuisson = (TextView) v.findViewById(R.id.tps_cuisson);
            short_content = (TextView) v.findViewById(R.id.short_content);
            date = (TextView) v.findViewById(R.id.date);
            image = (ImageView) v.findViewById(R.id.image);
            lyt_parent = (LinearLayout) v.findViewById(R.id.lyt_parent);
        }
    }

    class ViewHolderHeader extends RecyclerView.ViewHolder {
        public TextView title;
        public TextView cuisson;
        public TextView date;
        public ImageView image;
        public LinearLayout lyt_parent;

        public ViewHolderHeader(View v) {
            super(v);
            title = (TextView) v.findViewById(R.id.title);
            cuisson = (TextView) v.findViewById(R.id.tps_cuisson);
            date = (TextView) v.findViewById(R.id.date);
            image = (ImageView) v.findViewById(R.id.image);
            lyt_parent = (LinearLayout) v.findViewById(R.id.lyt_parent);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_ITEM) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_news_same, parent, false);
            return new ViewHolder(v);
        }else if (viewType == NATIVE_AD_VIEW_TYPE){
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_native_ad, parent, false);
            return new NativeAdViewHolder(v);
        }
        return null;
    }

    public class NativeAdViewHolder extends RecyclerView.ViewHolder {

        private final AdView mNativeAd;

        public NativeAdViewHolder(View itemView) {
            super(itemView);
            mNativeAd = (AdView) itemView.findViewById(R.id.adView);
            mNativeAd.setAdListener(new AdListener() {
                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                }

                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                }

                @Override
                public void onAdFailedToLoad(int errorCode) {
                    super.onAdFailedToLoad(errorCode);
                }

                @Override
                public void onAdLeftApplication() {
                    super.onAdLeftApplication();
                }

                @Override
                public void onAdOpened() {
                    super.onAdOpened();
                }
            });
            AdRequest adRequest = new AdRequest.Builder()
                    .addTestDevice("")
                    .build();
            mNativeAd.loadAd(adRequest);
        }
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        if (holder instanceof ViewHolderHeader) {
            ViewHolderHeader vHeader = (ViewHolderHeader) holder;
            vHeader.title.setText(header.getPosttitre());
            vHeader.cuisson.setText(header.getCuisson());
            vHeader.date.setText(header.getDate());
//            Picasso.with(ctx).load(header.getUrls().get(0).getUrl()).into(vHeader.image);
            Glide.with(vHeader.image.getContext()).load(header.getUrls().get(0).getUrl())
//                    .thumbnail(0.5f)
//                    .crossFade()
                    .fitCenter()

                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(new GlideDrawableImageViewTarget(vHeader.image) {
                        @Override
                        public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
                            super.onResourceReady(resource, animation);
                        }

                        @Override
                        public void onLoadFailed(Exception e, Drawable errorDrawable) {
                            super.onLoadFailed(e, errorDrawable);
                        }
                    });
            vHeader.lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, header, position);
                    }
                }
            });

        } else if (holder instanceof ViewHolder) {
            final News c = items.get(position);
            ViewHolder vItem = (ViewHolder) holder;
            vItem.title.setText(c.getPosttitre());
            vItem.cuisson.setText(c.getCuisson());
            vItem.short_content.setText(c.getPosttexte());
            vItem.date.setText(c.getDate());
//            Picasso.with(ctx).load(c.getUrls().get(0).getUrl()).into(vItem.image);
            Glide.with(ctx).load(c.getUrls().get(0).getUrl())
//                    .thumbnail(0.5f)
//                    .crossFade()
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(new GlideDrawableImageViewTarget(vItem.image) {
                        @Override
                        public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
                            super.onResourceReady(resource, animation);
                        }

                        @Override
                        public void onLoadFailed(Exception e, Drawable errorDrawable) {
                            super.onLoadFailed(e, errorDrawable);
                        }
                    });

            vItem.lyt_parent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (mOnItemClickListener != null) {
                        mOnItemClickListener.onItemClick(view, c, position);
                    }
                }
            });
        }

    }

    //    need to override this method
    @Override
    public int getItemViewType(int position) {
        if (position>1 && position % 5 == 0) {
            return NATIVE_AD_VIEW_TYPE;
        }
        return TYPE_ITEM;
    }

    private boolean isPositionHeader(int position) {
        return position == 0;
    }

    @Override
    public int getItemCount() {
        return filtered_items.size();
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String query = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();
            final List<News> list = items;
            final List<News> result_list = new ArrayList<>(list.size());
            for (int i = 0; i < list.size(); i++) {
                String str_title = list.get(i).getPosttexte();
                if (str_title.toLowerCase().contains(query)) {
                    result_list.add(list.get(i));
                }
            }
            results.values = result_list;
            results.count = result_list.size();
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filtered_items = (List<News>) results.values;
            notifyDataSetChanged();
        }

    }

}