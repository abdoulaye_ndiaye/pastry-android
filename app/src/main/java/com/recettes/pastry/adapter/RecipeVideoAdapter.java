package com.recettes.pastry.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.recettes.pastry.R;
import com.recettes.pastry.model.News;

import java.util.ArrayList;
import java.util.List;

public class RecipeVideoAdapter extends RecyclerView.Adapter<RecipeVideoAdapter.ViewHolder> implements Filterable {

    private List<News> original_items = new ArrayList<>();
    private List<News> filtered_items = new ArrayList<>();
    private ItemFilter mFilter = new ItemFilter();
    private static final int DEFAULT_VIEW_TYPE = 1;
    private static final int NATIVE_AD_VIEW_TYPE = 2;

    private Context ctx;

    // for item click listener
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, News obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView name;
        public TextView duration;
        public TextView category;
        public WebView webView;
        public MaterialRippleLayout lyt_parent;

        public ViewHolder(View v) {
            super(v);
            name = (TextView) v.findViewById(R.id.name);
            duration = (TextView) v.findViewById(R.id.duration);
            category = (TextView) v.findViewById(R.id.category);
            webView = v.findViewById(R.id.youtubePlayerView);
            lyt_parent = (MaterialRippleLayout) v.findViewById(R.id.lyt_parent);
        }
    }

    public Filter getFilter() {
        return mFilter;
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecipeVideoAdapter(Context ctx, List<News> items) {
        this.ctx = ctx;
        original_items = items;
        filtered_items = items;
    }

    @Override
    public int getItemViewType(int position) {
        if (position>1 && position % 4 == 0) {
            return NATIVE_AD_VIEW_TYPE;
        }
        return DEFAULT_VIEW_TYPE;
    }

    @Override
    public RecipeVideoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        final AdView mNativeAd;
        LayoutInflater layoutInflater = LayoutInflater.from(ctx);
//        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_video, parent, false);
//        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_news, parent, false);
//        ViewHolder vh = new ViewHolder(v);
        switch (viewType) {
            default:
                view = layoutInflater
                        .inflate(R.layout.item_video, parent, false);
                return new ViewHolder(view);
            case NATIVE_AD_VIEW_TYPE:
                view = layoutInflater.inflate(R.layout.list_item_video_native_ad, parent, false);
                mNativeAd = view.findViewById(R.id.adView);
                AdRequest adRequest = new AdRequest.Builder()
                        .addTestDevice("")
                        .build();
                mNativeAd.loadAd(adRequest);
                return new ViewHolder(view);
        }
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final News p = filtered_items.get(position);
        holder.category.setText(p.getCategory());
        holder.duration.setText(p.getCuisson());
        holder.name.setText(p.getPosttitre());
        if (p.getVideo()!= null && !p.getVideo().isEmpty()){
            holder.webView.setVisibility(View.VISIBLE);

            String frameVideo = "<iframe width='100%' height='100%' src=\"https://www.youtube.com/embed/"+p.getVideo()+"?autoplay=0\" frameborder=\"0\" allowfullscreen=\"allowfullscreen\"></iframe>";

            holder.webView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    return false;
                }
            });

            WebSettings webSettings = holder.webView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            holder.webView.loadData(frameVideo, "text/html", "utf-8");

        }

        setAnimation(holder.lyt_parent, position);
//        holder.lyt_parent.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (mOnItemClickListener != null) {
//                    mOnItemClickListener.onItemClick(view, p, position);
//                }
//            }
//        });
    }

    //Here is the key method to apply the animation
    private int lastPosition = -1;

    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(ctx, R.anim.slide_in_bottom);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return filtered_items.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String query = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();
            final List<News> list = original_items;
            final List<News> result_list = new ArrayList<>(list.size());
            for (int i = 0; i < list.size(); i++) {
                String str_title = list.get(i).getPosttitre();
                String str_cat = list.get(i).getCategory();
                if (str_title.toLowerCase().contains(query) || str_cat.toLowerCase().contains(query)) {
                    result_list.add(list.get(i));
                }
            }
            results.values = result_list;
            results.count = result_list.size();
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filtered_items = (List<News>) results.values;
            notifyDataSetChanged();
        }

    }
}