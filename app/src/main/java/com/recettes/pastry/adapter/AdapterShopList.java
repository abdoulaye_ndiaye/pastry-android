package com.recettes.pastry.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.recettes.pastry.R;
import com.recettes.pastry.model.Ingredien;
import com.recettes.pastry.model.News;
import com.recettes.pastry.model.Shop;

import java.util.ArrayList;
import java.util.List;
import static com.recettes.pastry.activity.BaseActivity.getUid;

public class AdapterShopList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Ingredien> items = new ArrayList<>();

    private Context ctx;
    private OnItemClickListener mOnItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, News obj, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mOnItemClickListener = mItemClickListener;
    }

    public AdapterShopList(Context context, List<Ingredien> items) {
        this.items = items;
        ctx = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView instruction;
        public ImageButton del;

        public ViewHolder(View v) {
            super(v);
            instruction = v.findViewById(R.id.id_instruction);
            del = v.findViewById(R.id.id_remove_item);
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_shoplist, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        String idUser = "";
        if (getUid() != null){
            idUser = getUid();
        }
        final DatabaseReference shopRef = FirebaseDatabase.getInstance().getReference("shop").child(idUser);
        final Ingredien ingregien = items.get(position);
        final ViewHolder vItem = (ViewHolder) holder;
        if (ingregien != null){
            vItem.instruction.setText(ingregien.getIngredien());
            vItem.del.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    vItem.instruction.setPaintFlags(vItem.instruction.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    shopRef.child(ingregien.getId()).child(ingregien.getPosition()+"").removeValue();
                    vItem.del.setBackgroundResource(R.color.grey_medium);
                }
            });
            vItem.instruction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    vItem.instruction.setPaintFlags(vItem.instruction.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                    shopRef.child(ingregien.getId()).child(ingregien.getPosition()+"").removeValue();
                    vItem.del.setBackgroundResource(R.color.grey_medium);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}