package com.recettes.pastry.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.recettes.pastry.R;

/**
 * Created by ghambyte on 15/04/2017.
 */

public class CommentViewHolder extends RecyclerView.ViewHolder {

    public TextView authorView;
    public ImageView authorPpView;
    public TextView bodyView;
    public TextView dateView;
    public LinearLayout cmtBackground;

    public CommentViewHolder(View itemView) {
        super(itemView);

        authorView = (TextView) itemView.findViewById(R.id.text_author);
        dateView = (TextView) itemView.findViewById(R.id.text_time);
        authorPpView = (ImageView) itemView.findViewById(R.id.image_status);
        bodyView = (TextView) itemView.findViewById(R.id.text_content);
        cmtBackground = (LinearLayout) itemView.findViewById(R.id.cmtBackground);
    }
}
