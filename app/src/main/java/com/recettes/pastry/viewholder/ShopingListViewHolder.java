package com.recettes.pastry.viewholder;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.recettes.pastry.R;
import com.recettes.pastry.activity.ActivityNewsDetails;
import com.recettes.pastry.adapter.AdapterIngredientList;
import com.recettes.pastry.adapter.AdapterShopList;
import com.recettes.pastry.controller.App;
import com.recettes.pastry.model.Ingredien;
import com.recettes.pastry.model.Shop;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.recettes.pastry.activity.BaseActivity.getUid;


public class ShopingListViewHolder extends RecyclerView.ViewHolder {

    public TextView id_instruction;
    public ImageView del_list;
    InterstitialAd mInterstitialAd;
    public static ProgressDialog mProgressDialog;
    AdapterShopList adapterIngredientList;
    public RecyclerView recyclerIngredients;

    public ShopingListViewHolder(View v) {
        super(v);

        id_instruction = v.findViewById(R.id.id_instruction);
        del_list = v.findViewById(R.id.del_list);
        recyclerIngredients = v.findViewById(R.id.recyclerIngredients);
    }

    public void bindToPost(final Ingredien shop, View.OnClickListener starClickListener,
                           View.OnClickListener userClickListener, View.OnClickListener commentClickListener,
                           View.OnClickListener photoClickListener) {
        mInterstitialAd = new InterstitialAd(getApplicationContext());
        mInterstitialAd.setAdUnitId("ca-app-pub-1607646997771772/3544951048");
        requestNewInterstitial();
        if (shop != null){
//            adapterIngredientList =  new AdapterShopList(id_instruction.getContext(), shop.getList(), shop.getInfos());
//            recyclerIngredients.setHasFixedSize(true);
//            recyclerIngredients.setLayoutManager(new LinearLayoutManager(id_instruction.getContext()));
//            recyclerIngredients.setAdapter(adapterIngredientList);
//            adapterIngredientList.notifyDataSetChanged();
            id_instruction.setText(shop.getIngredien());
//            id_instruction.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    App.nbrClicks++;
//                    if (mInterstitialAd.isLoaded() && App.nbrClicks > 3) {
//                        showProgressDialog();
//                        mInterstitialAd.show();
//                        mInterstitialAd.setAdListener(new AdListener() {
//                            @Override
//                            public void onAdClosed() {
//                                super.onAdClosed();
//                                App.nbrClicks = 0;
//                                hideProgressDialog();
//                                Intent intent = new Intent(id_instruction.getContext(), ActivityNewsDetails.class);
//                                intent.putExtra(ActivityNewsDetails.EXTRA_OBJC, shop.getInfos());
//                                id_instruction.getContext().startActivity(intent);
//                            }
//                        });
//
//                    } else {
//                        Intent intent = new Intent(id_instruction.getContext(), ActivityNewsDetails.class);
//                        intent.putExtra(ActivityNewsDetails.EXTRA_OBJC, shop.getInfos());
//                        id_instruction.getContext().startActivity(intent);
//                    }
//                }
//            });
        }
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("SEE_YOUR_LOGCAT_TO_GET_YOUR_DEVICE_ID")
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(id_instruction.getContext());
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Loading...");
        }

//        mProgressDialog.show();
    }

    public static void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
