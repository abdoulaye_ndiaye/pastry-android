package com.recettes.pastry.viewholder;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.recettes.pastry.activity.ActivityNewsDetails;
import com.recettes.pastry.R;
import com.recettes.pastry.controller.App;
import com.recettes.pastry.model.News;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import static com.facebook.FacebookSdk.getApplicationContext;


public class PostViewHolder extends RecyclerView.ViewHolder {

    public TextView title;
    public TextView short_content;
    public TextView date;
    public TextView tps_cuisson;
    public ImageView image;
    public LinearLayout lyt_parent;
    public LinearLayout aditem;
    public AdView mAdView;
    InterstitialAd mInterstitialAd;
    public static ProgressDialog mProgressDialog;

    public PostViewHolder(View v) {
        super(v);

        title = v.findViewById(R.id.title);
        aditem = v.findViewById(R.id.aditem);
        mAdView = v.findViewById(R.id.adView);
        short_content = v.findViewById(R.id.short_content);
        date = v.findViewById(R.id.date);
        tps_cuisson = v.findViewById(R.id.tps_cuisson);
        image = v.findViewById(R.id.image);
        lyt_parent = v.findViewById(R.id.lyt_parent);
    }

    public void bindToPost(final News post, View.OnClickListener starClickListener,
                           View.OnClickListener userClickListener, View.OnClickListener commentClickListener,
                           View.OnClickListener photoClickListener) {
        mInterstitialAd = new InterstitialAd(getApplicationContext());
        mInterstitialAd.setAdUnitId("ca-app-pub-1607646997771772/3544951048");
        requestNewInterstitial();
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        if (post != null){
            if (post.getUrls().get(0) != null){
                title.setText(post.getPosttitre());
                date.setText(post.getDate());
                tps_cuisson.setText(post.getCuisson());
                Glide.with(image.getContext()).load(post.getUrls().get(0).getUrl())
                        .fitCenter()

                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(new GlideDrawableImageViewTarget(image) {
                            @Override
                            public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
                                super.onResourceReady(resource, animation);
                            }

                            @Override
                            public void onLoadFailed(Exception e, Drawable errorDrawable) {
                                super.onLoadFailed(e, errorDrawable);
                            }
                        });
                lyt_parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        App.nbrClicks++;
                        if (mInterstitialAd.isLoaded() && App.nbrClicks > 3) {
                            showProgressDialog();
                            mInterstitialAd.show();
                            mInterstitialAd.setAdListener(new AdListener() {
                                @Override
                                public void onAdClosed() {
                                    super.onAdClosed();
                                    App.nbrClicks = 0;
                                    hideProgressDialog();
                                    Intent intent = new Intent(lyt_parent.getContext(), ActivityNewsDetails.class);
                                    intent.putExtra(ActivityNewsDetails.EXTRA_OBJC, post);
                                    lyt_parent.getContext().startActivity(intent);
                                }
                            });

                        } else {
                            Intent intent = new Intent(lyt_parent.getContext(), ActivityNewsDetails.class);
                            intent.putExtra(ActivityNewsDetails.EXTRA_OBJC, post);
                            lyt_parent.getContext().startActivity(intent);
                        }
                    }
                });
            }
        }
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("SEE_YOUR_LOGCAT_TO_GET_YOUR_DEVICE_ID")
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(lyt_parent.getContext());
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage("Loading...");
        }

//        mProgressDialog.show();
    }

    public static void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
