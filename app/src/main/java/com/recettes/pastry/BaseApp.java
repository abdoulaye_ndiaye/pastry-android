package com.recettes.pastry;

import android.support.multidex.MultiDexApplication;

import com.recettes.pastry.model.User;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import retrofit2.Retrofit;

/**
 * Created by ghambyte on 10/05/2017 at 23:10.
 * Project name : Modelic-V2
 */

public class BaseApp extends MultiDexApplication {

    public static Retrofit mobileAdapter;
    public static FirebaseDatabase database;
    public static DatabaseReference mDatabase;
    public static String photo1;
    public static User authenticatedUser = null;

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(getApplicationContext());
        database = FirebaseDatabase.getInstance();
        database.setPersistenceEnabled(true);
        mDatabase =  database.getReference();
    }
}
