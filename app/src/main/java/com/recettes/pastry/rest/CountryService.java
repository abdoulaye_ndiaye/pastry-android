package com.recettes.pastry.rest;


import com.recettes.pastry.model.UserCountryInfos;

import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by ghambyte on 07/04/2018 at 16:42.
 * Project name : Modelic
 */

public interface CountryService {
    @GET("http://ip-api.com/json/")
    Call<UserCountryInfos> getInfos();
}
