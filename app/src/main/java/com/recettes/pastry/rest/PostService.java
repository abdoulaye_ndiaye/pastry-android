package com.recettes.pastry.rest;

import com.recettes.pastry.model.NewsResp;
import com.recettes.pastry.model.PostItem;
import com.recettes.pastry.model.PostResp;
import com.recettes.pastry.model.ResponseBase;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by ghambyte on 25/04/2017 at 17:26.
 * Project name : Modelic-V2
 */
public interface PostService {

    @GET("/lespostes")
    Call<PostResp> postlist(@Query("authorid") String authorid, @Query("category") String category,
                            @Query("sexe") String sexe, @Query("photouser") String photouser, @Query("page") int page);
    @GET("/videos")
    Call<PostResp> videos(@Query("authorid") String authorid, @Query("category") String category,
                            @Query("sexe") String sexe, @Query("photouser") String photouser, @Query("page") int page);

    @GET("/post/{id}")
    Call<NewsResp> post(@Path("id") String id);

    @POST("/dopost")
    Call<ResponseBase> doPost(@Body PostItem postItem);

    @POST("/deletePost/{id}")
    Call<PostResp> deletePost(@Path("id") int id);
}
