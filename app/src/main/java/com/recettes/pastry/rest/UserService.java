package com.recettes.pastry.rest;

import com.recettes.pastry.model.ResponseBase;
import com.recettes.pastry.model.Client;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by ghambyte on 25/04/2017 at 17:26.
 * Project name : Modelic-V2
 */
public interface UserService {

    @POST("/postUser")
    Call<ResponseBase> postUser(@Body Client client);
}
