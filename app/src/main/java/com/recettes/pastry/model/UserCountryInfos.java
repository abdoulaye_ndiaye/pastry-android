package com.recettes.pastry.model;

/**
 * Created by ghambyte on 07/04/2018 at 16:44.
 * Project name : Tamweel
 */

public class UserCountryInfos {

    private String ip;
    private String country;
    private String time_zone;

    public UserCountryInfos(String ip, String country_code, String country_name, String time_zone) {
        this.ip = ip;
        this.country = country_code;
        this.time_zone = time_zone;
    }

    public UserCountryInfos() {
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }



    public String getTime_zone() {
        return time_zone;
    }

    public void setTime_zone(String time_zone) {
        this.time_zone = time_zone;
    }

    @Override
    public String toString() {
        return "UserCountryInfos{" +
                "ip='" + ip + '\'' +
                ", country='" + country + '\'' +
                ", time_zone='" + time_zone + '\'' +
                '}';
    }
}
