package com.recettes.pastry.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by ghambyte on 16/11/2017 at 11:17.
 * Project name : BGFIMobile
 */

public class PhotoModel implements Serializable, Parcelable{

    String titre;

    String url;

    public PhotoModel() {
    }

    public PhotoModel(String titre, String url) {
        this.titre = titre;
        this.url = url;
    }

    protected PhotoModel(Parcel in) {
        titre = in.readString();
        url = in.readString();
    }

    public static final Creator<PhotoModel> CREATOR = new Creator<PhotoModel>() {
        @Override
        public PhotoModel createFromParcel(Parcel in) {
            return new PhotoModel(in);
        }

        @Override
        public PhotoModel[] newArray(int size) {
            return new PhotoModel[size];
        }
    };

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "PhotoModel{" +
                "titre='" + titre + '\'' +
                ", url='" + url + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.titre);
        parcel.writeString(this.url);
    }
}
