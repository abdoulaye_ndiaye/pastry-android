package com.recettes.pastry.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by ghambyte on 16/11/2017 at 11:17.
 * Project name : BGFIMobile
 */

public class VenteModel implements Serializable, Parcelable{

    String titre;

    String url;

    public VenteModel() {
    }

    public VenteModel(String titre, String url) {
        this.titre = titre;
        this.url = url;
    }

    protected VenteModel(Parcel in) {
        titre = in.readString();
        url = in.readString();
    }

    public static final Creator<VenteModel> CREATOR = new Creator<VenteModel>() {
        @Override
        public VenteModel createFromParcel(Parcel in) {
            return new VenteModel(in);
        }

        @Override
        public VenteModel[] newArray(int size) {
            return new VenteModel[size];
        }
    };

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "Vente{" +
                "titre='" + titre + '\'' +
                ", url='" + url + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(this.titre);
        parcel.writeString(this.url);
    }
}
