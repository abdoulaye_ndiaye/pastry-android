package com.recettes.pastry.model;

/**
 * Created by ghambyte on 01/05/2017 at 11:08.
 * Project name : Modelic-V2
 */

public class Favorit {

    String id;

    String url;

    String author;

    String authorid;

    public Favorit(String id, String url, String author, String authorid) {
        this.id = id;
        this.url = url;
        this.author = author;
        this.authorid = authorid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthorid() {
        return authorid;
    }

    public void setAuthorid(String authorid) {
        this.authorid = authorid;
    }
}
