package com.recettes.pastry.model;

/**
 * Created by ghambyte on 20/11/2017 at 12:23.
 * Project name : BGFIMobile
 */

public class NewsResp {

    String code;
    News post;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public News getPost() {
        return post;
    }

    public void setPost(News post) {
        this.post = post;
    }

    @Override
    public String toString() {
        return "NewsResp{" +
                "code='" + code + '\'' +
                ", post=" + post +
                '}';
    }
}
