package com.recettes.pastry.model;

/**
 * Created by ghambyte on 07/11/2018 at 16:10.
 * Project name : Tamweel
 */

public class Ingredien {

    String ingredien;
    String id;
    int position;

    public Ingredien() {
    }

    public Ingredien(String ingredien, String id, int position) {
        this.ingredien = ingredien;
        this.id = id;
        this.position = position;
    }

    public String getIngredien() {
        return ingredien;
    }

    public void setIngredien(String ingredien) {
        this.ingredien = ingredien;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "Ingredien{" +
                "ingredien='" + ingredien + '\'' +
                ", id='" + id + '\'' +
                ", position='" + position + '\'' +
                '}';
    }
}
