package com.recettes.pastry.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ghambyte on 07/11/2018 at 10:56.
 * Project name : Tamweel
 */
@IgnoreExtraProperties
public class Shop implements Serializable {

    News infos;
    List<Ingredien> list;

    public Shop() {
    }

    public Shop(News infos, List<Ingredien> list) {
        this.infos = infos;
        this.list = list;
    }

    public News getInfos() {
        return infos;
    }

    public void setInfos(News infos) {
        this.infos = infos;
    }

    public List<Ingredien> getList() {
        return list;
    }

    public void setList(List<Ingredien> list) {
        this.list = list;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("infos", infos);
        result.put("list", list);

        return result;
    }
}
