package com.recettes.pastry.model;

import java.io.Serializable;

/**
 * Created by ghambyte on 25/04/2017.
 */

public class PostItem implements Serializable {

    String id;

    String author;

    String authorid;

    String url;

    String category;

    String posttitre;

    String posttexte;

    String photouser;

    String pays;

    String telephone;

    String date;

    String ingredients;

    String urlImage;

    String postid;

    public PostItem() {
    }

    public PostItem(String category, String titre) {
        this.category = category;
        this.posttitre = titre;
    }

    public PostItem(String authorId, String category, String titre) {
        this.category = category;
        this.posttitre = titre;
        this.authorid = authorId;
    }

    public PostItem(String postid, String author, String authorid, String url, String category, String titre, String posttexte,
                    String photoUser, String pays, String telephone, String date, String ingredients, String urlImage) {
        this.postid = postid;
        this.author = author;
        this.authorid = authorid;
        this.url = url;
        this.category = category;
        this.posttitre = titre;
        this.posttexte = posttexte;
        this.photouser = photoUser;
        this.pays = pays;
        this.telephone = telephone;
        this.date = date;
        this.ingredients = ingredients;
        this.urlImage = urlImage;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitre() {
        return posttitre;
    }

    public void setTitre(String titre) {
        this.posttitre = titre;
    }

    public String getAuthorid() {
        return authorid;
    }

    public void setAuthorid(String authorid) {
        this.authorid = authorid;
    }

    public String getMore() {
        return posttexte;
    }

    public void setMore(String more) {
        this.posttexte = more;
    }

    public String getPhotouser() {
        return photouser;
    }

    public void setPhotouser(String photouser) {
        this.photouser = photouser;
    }

    public String getPays() {
        return pays;
    }

    public void setPays(String pays) {
        this.pays = pays;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public String toString() {
        return "PostItem{" +
                "id='" + id + '\'' +
                ", author='" + author + '\'' +
                ", authorid='" + authorid + '\'' +
                ", url='" + url + '\'' +
                ", category='" + category + '\'' +
                ", titre='" + posttitre + '\'' +
                '}';
    }
}
