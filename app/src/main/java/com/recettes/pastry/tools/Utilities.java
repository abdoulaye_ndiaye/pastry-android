package com.recettes.pastry.tools;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v7.app.AlertDialog;
import android.util.Pair;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.io.ByteArrayOutputStream;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;

public class Utilities {

    private static String KEY_LIS_ID = "KEY_LIS_ID";

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static String[] getMonths(@NonNull String periode) {
        String[] res = periode.toUpperCase().split("T");
        if (res.length != 2) {
            return null;
        }
        switch (Integer.parseInt(res[0])) {
            case 1:
                return new String[]{"Janvier", "Fevrier", "Mars"};
            case 2:
                return new String[]{"Avril", "Mai", "Juin"};
            case 3:
                return new String[]{"Juillet", "Aout", "Septembre"};
            case 4:
                return new String[]{"Octobre", "Novembre", "Decembre"};
            default:
                return null;
        }
    }

    public static boolean isIdExist(Context ctx, String id){
        List<String> listId = getListId(ctx);
        for (int i = 0; i < listId.size(); i++) {
            if(listId.get(i).equals(id)){
                return true;
            }
        }
        return false;
    }

    private static List<String> getListId(Context context){
        String s = getStringPref(KEY_LIS_ID, "null" , context);
        return split(s);
    }

    private static String getStringPref(String key_val, String def_val, Context context) {
        SharedPreferences pref = context.getSharedPreferences("pref_" + key_val, context.MODE_PRIVATE);
        return pref.getString(key_val, def_val);
    }

    private static List<String> split(String s){
        // split by "|"
        List<String> list = new ArrayList<>();
        if(!s.equals("null")){
            for (int i = 0; i < s.split("\\|").length; i++) {
                list.add(s.split("\\|")[i]);
            }
        }
        return list;
    }

    public static void hideKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

    }

    /*public static String[] explode(String str, String del) {
        Iterable<String> iterator = Splitter.on(del).trimResults().omitEmptyStrings()
                .split(str);
        return Iterables.toArray(iterator, String.class);
    }*/

    public static String getMonth(int number) {
        switch (number) {
            case 1:
                return "Janvier";
            case 2:
                return "Fevrier";
            case 3:
                return "Mars";
            case 4:
                return "Avril";
            case 5:
                return "Mai";
            case 6:
                return "Juin";
            case 7:
                return "Juillet";
            case 8:
                return "Aout";
            case 9:
                return "Septembre";
            case 10:
                return "Octobre";
            case 11:
                return "Novembre";
            case 12:
                return "Decembre";
            default:
                return "";
        }
    }

    public static String getSemestre(@NonNull String periode) {
        String[] res = periode.toUpperCase().split("T");
        if (res.length != 2) {
            return null;
        }
        switch (Integer.parseInt(res[0])) {
            case 1:
                return "Premier semestre";
            case 2:
                return "Deuxieme semestre";
            case 3:
                return "Troisieme semestre";
            case 4:
                return "Quatrieme semestre";
            default:
                return null;
        }

    }

    /**
     * Savoir si une connexion internet est disponible
     */
    public static boolean isInternetAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    /**
     * Convertir un paire Value en bundle
     *
     * @return retour du bundle
     */
    public static Bundle paireValueToBundle(ArrayList<Pair<String, String>> values) {
        ArrayList<String> keys = new ArrayList<>();
        ArrayList<String> stringValues = new ArrayList<>();

        Bundle bundle = new Bundle();
        if (values != null) {
            for (Pair<String, String> valuePair : values) {
                if (!keys.contains(valuePair.first) && !keys.contains(valuePair.second)) {
                    keys.add(valuePair.first);
                    stringValues.add(valuePair.second);
                }
            }
        }
        bundle.putStringArrayList("key", keys);
        bundle.putStringArrayList("value", stringValues);
        return bundle;
    }

    public static void showMessage(String title, String message, Context context) {
        if (context == null) {
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        if (message == null) {
            return;
        }

        builder.setMessage(message);

        if (title != null) {
            builder.setTitle(title);
        }

        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static void showMessage(@StringRes int title, String message, Context context) {
        if (context == null) {
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        if (message == null) {
            return;
        }

        builder.setMessage(message);
        builder.setTitle(title);
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static void showMessage(@StringRes int title, @StringRes int message, Context context) {
        if (context == null) {
            return;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
        builder.setTitle(title);
        builder.setCancelable(false);
        builder.setPositiveButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public static String formatAmount(String montant, String separator) {
        return montant.trim().replace(separator, "");
    }

    public static String normalizeText(String original) {
        String nfdNormalizedString = Normalizer.normalize(original, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("");
    }

    public interface OnDataLoadFinish {

        void onFinished(String data);
    }

    public static String getUserCountry(Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("DATA",Context.MODE_PRIVATE);
        String id = sharedPreferences.getString("COUNTRY",null);
        return id;
    }

    public static void setUserCountry(String country, Context context){
        SharedPreferences sharedPreferences = context.getSharedPreferences("DATA",Context.MODE_PRIVATE);
        sharedPreferences.edit().putString("COUNTRY",country).apply();
    }

    public static String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }

}
