package com.recettes.pastry.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.recettes.pastry.activity.ActivityMain;
import com.recettes.pastry.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ghambyte on 28/11/2016.
 */

public class ProfileUploadService extends MyBaseTaskService {

    private static final String TAG = "MyUploadService";
    private static final int NOTIF_ID_DOWNLOAD = 0;

    /** Intent Actions **/
    public static final String ACTION_UPLOAD = "action_upload";
    public static final String UPLOAD_COMPLETED = "upload_completed";
    public static final String UPLOAD_ERROR = "upload_error";

    /** Intent Extras **/
    public static final String EXTRA_FILE_URI = "extra_file_uri";
    public static final String EXTRA_DOWNLOAD_URL = "extra_download_url";

    // [START declare_ref]
    private StorageReference mStorageRef;
    private DatabaseReference mUserReference;
    private DatabaseReference mCommentReference;
    private DatabaseReference mUserPostReference;
    private DatabaseReference mPostReference;
    Uri uriFile = null;
    // [END declare_ref]

    @Override
    public void onCreate() {
        super.onCreate();

        // [START get_storage_ref]
        mStorageRef = FirebaseStorage.getInstance().getReference();
        // [END get_storage_ref]
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand:" + intent + ":" + startId);
        if (ACTION_UPLOAD.equals(intent.getAction())) {
            Uri fileUri = intent.getParcelableExtra(EXTRA_FILE_URI);
            uploadFromUri(fileUri);
        }

        return START_REDELIVER_INTENT;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                                   boolean filter) {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }

    public Bitmap loadBitmap(String url)
    {
        Bitmap bm = null;
        InputStream is = null;
        BufferedInputStream bis = null;
        try
        {
            URLConnection conn = new URL(url).openConnection();
            conn.connect();
            is = conn.getInputStream();
            bis = new BufferedInputStream(is, 8192);
            bm = BitmapFactory.decodeStream(bis);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally {
            if (bis != null)
            {
                try
                {
                    bis.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
            if (is != null)
            {
                try
                {
                    is.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
        return bm;
    }

    // [START upload_from_uri]
    private void uploadFromUri(final Uri fileUri) {
        Log.d(TAG, "uploadFromUri:src:" + fileUri.toString());
        uriFile = fileUri;
        Runnable r = new Runnable() {
            public void run() {
                byte[] imageInByte = null;
                Bitmap b = null;
                try {
                    b = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(),fileUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                Bitmap bitmap = b;
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                imageInByte = stream.toByteArray();
                long lengthbmp = imageInByte.length;
                if (lengthbmp > 200000){
                    do {
                        bitmap = Bitmap.createScaledBitmap(bitmap,(int)(bitmap.getWidth()*0.5), (int)(bitmap.getHeight()*0.5), true);
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
                        imageInByte = stream.toByteArray();
                        lengthbmp = imageInByte.length;
                    } while (lengthbmp < 200000);
                    uriFile = getImageUri(getApplicationContext(), bitmap);
                }
                taskStarted();
                showUploadProgressNotification();
                final StorageReference photoRef = mStorageRef.child("photos")
                        .child(uriFile.getLastPathSegment());
//                Toast.makeText(getApplicationContext(), uriFile.getLastPathSegment(), Toast.LENGTH_LONG).show();
                Log.d(TAG, "uploadFromUri:dst:" + photoRef.getPath());
                photoRef.putFile(uriFile)
                        .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                            @Override
                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                // Upload succeeded
                                Log.d(TAG, "uploadFromUri:onSuccess");

                                // Get the public download URL
//                                Uri downloadUri = taskSnapshot.getMetadata().getDownloadUrl();
//
//                                // [START_EXCLUDE]
////                        broadcastUploadFinished(downloadUri, fileUri);
//                                showUploadFinishedNotification(downloadUri, uriFile);
//                                taskCompleted();
                                // [END_EXCLUDE]
                            }
                        })
                        .addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception exception) {
                                // Upload failed
                                Log.w(TAG, "uploadFromUri:onFailure", exception);

                                // [START_EXCLUDE]
                                broadcastUploadFinished(null, uriFile);
                                showUploadFinishedNotification(null, uriFile);
                                taskCompleted();
                                // [END_EXCLUDE]
                            }
                        });
            }
        };

        new Thread(r).start();
//        taskStarted();

    }
    // [END upload_from_uri]

    /**
     * Broadcast finished upload (success or failure).
     * @return true if a running receiver received the broadcast.
     */
    private boolean broadcastUploadFinished(@Nullable Uri downloadUrl, @Nullable Uri fileUri) {
        boolean success = downloadUrl != null;

        String action = success ? UPLOAD_COMPLETED : UPLOAD_ERROR;

        Intent broadcast = new Intent(action)
                .putExtra(EXTRA_DOWNLOAD_URL, downloadUrl)
                .putExtra(EXTRA_FILE_URI, fileUri);
        return LocalBroadcastManager.getInstance(getApplicationContext())
                .sendBroadcast(broadcast);
    }

    /**
     * Show a notification for a finished upload.
     */
    private void showUploadFinishedNotification(@Nullable Uri downloadUrl, @Nullable Uri fileUri) {
        final String uid = getUid();
//         Make Intent to NewPostActivity
        Intent intent = new Intent(getApplicationContext(), ActivityMain.class)
//                .putExtra(EXTRA_DOWNLOAD_URL, downloadUrl.toString())
//                .putExtra(EXTRA_FILE_URI, fileUri)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(intent);

        // Make PendingIntent for notification
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0 /* requestCode */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        // Set message and icon based on success or failure
        boolean success = downloadUrl != null;
        String message = success ? "Photo enregistré!" : "Upload failed";
        int icon = success ? R.drawable.ic_action_gmail : R.drawable.ic_action_gmail;

        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(icon)
                .setContentTitle(getString(R.string.app_name))
                .setContentText(message)
                .setAutoCancel(true)
                .setContentIntent(pendingIntent);

        NotificationManager manager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        manager.notify(NOTIF_ID_DOWNLOAD, builder.build());
        mUserReference = FirebaseDatabase.getInstance().getReference().child("users").child(uid);
        final Map<String, Object> updates = new HashMap<String, Object>();
        final Map<String, Object> updateComment = new HashMap<String, Object>();
        updates.put("pprofile", downloadUrl.toString());
        updateComment.put("authorPp", downloadUrl.toString());
//                Picasso.with(ProfileActivity.this).load(uri).into(imageProfile);
//        Glide.with(getApplicationContext()).load(uri)
////                    .thumbnail(0.5f)
////                    .crossFade()
//                .centerCrop()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(new GlideDrawableImageViewTarget(imageProfile) {
//                    @Override
//                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
//                        super.onResourceReady(resource, animation);
//                    }
//
//                    @Override
//                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
//                        super.onLoadFailed(e, errorDrawable);
//                        //never called
//                    }
//                });
        mUserReference.updateChildren(updates);
        FirebaseDatabase.getInstance().getReference().child("user-posts").child(uid)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        for (final DataSnapshot postSnapshot: dataSnapshot.getChildren()) {
                            mUserPostReference = FirebaseDatabase.getInstance().getReference().child("user-posts").child(uid).child(postSnapshot.getKey());
                            mUserPostReference.updateChildren(updates);
                            FirebaseDatabase.getInstance().getReference().child("models").child(uid)
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
//                                                        postSnapshot.getKey();
                                            mPostReference = FirebaseDatabase.getInstance().getReference().child("models").child(postSnapshot.getKey());
                                            mPostReference.updateChildren(updates);
//                                            mCommentReference = FirebaseDatabase.getInstance().getReference().child("comments").child(postSnapshot.getKey());
                                            FirebaseDatabase.getInstance().getReference().child("comments").child(postSnapshot.getKey())
//                                            FirebaseDatabase.getInstance().getReference().child("comments")
                                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                                        @Override
                                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                                            for (DataSnapshot imageSnapshot: dataSnapshot.getChildren()) {
                                                                FirebaseDatabase.getInstance().getReference().child("comments").child(imageSnapshot.getKey())
                                                                        .addListenerForSingleValueEvent(new ValueEventListener() {
                                                                            @Override
                                                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                                                for (DataSnapshot snapshot: dataSnapshot.getChildren()){
                                                                                    try {
//                                                                                        Comment comment = snapshot.getValue(Comment.class);
//                                                                                        if (comment!=null){
//                                                                                            if (comment.uid.equals(uid)){
//                                                                                                FirebaseDatabase.getInstance().getReference().child("comments").child(dataSnapshot.getKey()).child(snapshot.getKey()).updateChildren(updateComment);
//                                                                                            }
//                                                                                        }
                                                                                    }catch (DatabaseException error){
                                                                                        System.out.println(error);
                                                                                    }
                                                                                }
                                                                            }

                                                                            @Override
                                                                            public void onCancelled(DatabaseError databaseError) {

                                                                            }
                                                                        });
                                                            }
                                                        }

                                                        @Override
                                                        public void onCancelled(DatabaseError databaseError) {

                                                        }
                                                    });
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    public String getUid() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    /**
     * Show notification with an indeterminate upload progress bar.
     */
    private void showUploadProgressNotification() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_action_gmail)
                .setContentTitle(getString(R.string.app_name))
                .setContentText("Enregistrement photo...")
                .setProgress(0, 0, true)
                .setOngoing(true)
                .setAutoCancel(false);

        NotificationManager manager =
                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        manager.notify(NOTIF_ID_DOWNLOAD, builder.build());
    }

    public static IntentFilter getIntentFilter() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(UPLOAD_COMPLETED);
        filter.addAction(UPLOAD_ERROR);

        return filter;
    }

}