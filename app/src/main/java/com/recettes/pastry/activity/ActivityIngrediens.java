package com.recettes.pastry.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.recettes.pastry.R;
import com.recettes.pastry.service.MyUploadService;

import java.util.ArrayList;

import static com.recettes.pastry.service.MyUploadService.EXTRA_FILE_URI;
import static com.recettes.pastry.service.MyUploadService.EXTRA_FILE_URI1;
import static com.recettes.pastry.service.MyUploadService.EXTRA_FILE_URI2;
import static com.recettes.pastry.service.MyUploadService.EXTRA_FILE_URI3;
import static com.recettes.pastry.service.MyUploadService.EXTRA_FILE_URI4;
import static com.recettes.pastry.service.MyUploadService.EXTRA_FILE_URI5;

/**
 * Created by ghambyte on 29/05/2018 at 11:13.
 * Project name : Tamweel
 */

public class ActivityIngrediens extends BaseActivity {

    EditText editText;
    Spinner spinnerCategory;
    ImageView addButton;
    Button suivantRecette;
    ListView listView;
    ArrayList<String> listItems;
    ArrayAdapter<String> adapter;
    String ingredient = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingrediens);
        Toolbar mToolBar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Intent intent = getIntent();
        editText = findViewById(R.id.editText);
        spinnerCategory = findViewById(R.id.spinner_category);
        addButton = findViewById(R.id.addItem);
        suivantRecette = findViewById(R.id.id_recette_text_button);
        listView = findViewById(R.id.listView);
        listItems = new ArrayList<String>();
//        listItems.add("First Item - added on Activity Create");
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, listItems);
        listView.setAdapter(adapter);
        final Uri fileUri = intent.getParcelableExtra(EXTRA_FILE_URI);
        final Uri fileUri1 = intent.getParcelableExtra(EXTRA_FILE_URI1);
        final Uri fileUri2 = intent.getParcelableExtra(EXTRA_FILE_URI2);
        final Uri fileUri3 = intent.getParcelableExtra(EXTRA_FILE_URI3);
        final Uri fileUri4 = intent.getParcelableExtra(EXTRA_FILE_URI4);
        final Uri fileUri5 = intent.getParcelableExtra(EXTRA_FILE_URI5);
        addButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                listItems.add(editText.getText().toString());
                suivantRecette.setVisibility(View.VISIBLE);
                adapter.notifyDataSetChanged();
                editText.setText(null);
                listView.post(new Runnable() {
                    @Override
                    public void run() {
                        // Call smooth scroll
                        listView.smoothScrollToPosition(listItems.size());
                    }
                });
            }
        });
        hideKeyboard();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> a, View v, int position,
                                    long id) {
                Toast.makeText(ActivityIngrediens.this, "Clicked", Toast.LENGTH_LONG)
                        .show();
            }
        });
        suivantRecette.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < listItems.size(); i++) {
                    if (i != listItems.size()-1){
                        ingredient = ingredient + listItems.get(i) + ",";
                    }else {
                        ingredient = ingredient + listItems.get(i);
                    }
                    System.out.println(listItems.get(i));
                }
                Intent myIntent = new Intent(ActivityIngrediens.this, ActivityLaRecette.class);
                myIntent.putExtra("category", spinnerCategory.getSelectedItem().toString());
                myIntent.putExtra("ingredients", ingredient);
                myIntent.putExtra(MyUploadService.EXTRA_FILE_URI, fileUri);
                myIntent.putExtra(MyUploadService.EXTRA_FILE_URI1, fileUri1);
                myIntent.putExtra(MyUploadService.EXTRA_FILE_URI2, fileUri2);
                myIntent.putExtra(MyUploadService.EXTRA_FILE_URI3, fileUri3);
                myIntent.putExtra(MyUploadService.EXTRA_FILE_URI4, fileUri4);
                myIntent.putExtra(MyUploadService.EXTRA_FILE_URI5, fileUri5);
                ActivityIngrediens.this.startActivity(myIntent);
            }
        });

        editText.addTextChangedListener(contentWatcher);
        if (editText.length() == 0) {
            addButton.setEnabled(false);
        }
    }

    private TextWatcher contentWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable etd) {
            if (etd.toString().trim().length() == 0) {
                addButton.setEnabled(false);
            } else {
                addButton.setEnabled(true);
            }
            //draft.setContent(etd.toString());
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }
    };

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm != null) {
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }
}
