package com.recettes.pastry.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.recettes.pastry.BaseApp;
import com.recettes.pastry.R;
import com.recettes.pastry.adapter.RecipeGridAdapter;
import com.recettes.pastry.controller.App;
import com.recettes.pastry.data.Category;
import com.recettes.pastry.data.Tools;
import com.recettes.pastry.model.News;
import com.recettes.pastry.model.PostResp;
import com.recettes.pastry.rest.PostService;
import com.recettes.pastry.tools.Utilities;
import com.recettes.pastry.view.EndlessRecyclerOnScrollListener;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityBaseCategoryDetails extends AppCompatActivity {

    public static final String EXTRA_OBJCT = "com.app.sample.recipe.OBJ";

    // give preparation animation activity transition
    public static void navigate(AppCompatActivity activity, View transitionImage, Category obj) {
        Intent intent = new Intent(activity, ActivityBaseCategoryDetails.class);
        intent.putExtra(EXTRA_OBJCT, obj);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitionImage, EXTRA_OBJCT);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }

    private RecyclerView recyclerView;
    private Category category;
    private View parent_view;
    private ProgressBar progressbar;
    List<News> postItemList;
    TextView text_empry_list;
    ImageButton tryAgain;
    private int nextPage = 1;
    PostService postService = BaseApp.mobileAdapter.create(PostService.class);
    public static RecipeGridAdapter mAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    private SearchView searchView;
    private boolean hasPage = true;
    InterstitialAd mInterstitialAd;
    Context context;
    public static ProgressDialog mProgressDialog;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_details);
        parent_view = findViewById(android.R.id.content);

        category = (Category) getIntent().getSerializableExtra(EXTRA_OBJCT);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(category.getName());

        recyclerView = findViewById(R.id.recyclerView);
        LinearLayoutManager mLayoutManager = new GridLayoutManager(this, Tools.calculateNoOfColumns(this));
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView = findViewById(R.id.recyclerView);
        progressbar = findViewById(R.id.progressbar);
        text_empry_list = findViewById(R.id.text_empry_list);
        tryAgain = findViewById(R.id.try_again);
        swipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        mInterstitialAd = new InterstitialAd(getApplicationContext());
        mInterstitialAd.setAdUnitId(getString(R.string.str_interstitial));
        requestNewInterstitial();
        getclientInfos("basic recipes/"+category.getName().toLowerCase());
        tryAgain.setOnClickListener(tryAgainListener);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                nextPage = 1;
                if (mInterstitialAd.isLoaded()){
//                    showProgressDialog();
                    mInterstitialAd.show();
                    mInterstitialAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdClosed() {
                            super.onAdClosed();
                            hideProgressDialog();
                            getclientInfos("basic recipes/"+category.getName().toLowerCase());
                        }
                    });
                }else {
                    getclientInfos("basic recipes/"+category.getName().toLowerCase());
                }
            }
        });
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("SEE_YOUR_LOGCAT_TO_GET_YOUR_DEVICE_ID")
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

    private View.OnClickListener tryAgainListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            progressbar.setVisibility(View.VISIBLE);
            tryAgain.setVisibility(View.GONE);
            nextPage = 1;
            getclientInfos("");
        }
    };

    private void getclientInfos(String category) {
        if (!Utilities.isInternetAvailable(getApplicationContext())) {
            progressbar.setVisibility(View.GONE);
            Utilities.showMessage(null, getString(R.string.no_internet), context);
            tryAgain.setVisibility(View.VISIBLE);
            return;
        }
        progressbar.setVisibility(View.VISIBLE);
        Call<PostResp> operations;
        operations = postService.postlist("", category, "", "", nextPage);
        operations.enqueue(callback);
    }

    private Callback<PostResp> page = new Callback<PostResp>() {
        @Override
        public void onResponse(Call<PostResp> call, Response<PostResp> res) {
            progressbar.setVisibility(View.VISIBLE);
            int code = res.code();
            if (code == 200) {
                postItemList.addAll(res.body().getPostItems());
                mAdapter.notifyDataSetChanged();

                progressbar.setVisibility(View.GONE);
            }
            if (code == 401) {
                ResponseBody base = res.errorBody();
                if (base != null) {
                    //TODO Client inconnu. Redirect to login screen
                    Toast.makeText(getApplicationContext(), res.message(), Toast.LENGTH_SHORT)
                            .show();
                }
                finish();
            }
            if (code == 500) {
                Toast.makeText(getApplicationContext(), R.string.str_unknown_error, Toast.LENGTH_SHORT)
                        .show();
            }
            progressbar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PostResp> call, Throwable t) {
            progressbar.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            tryAgain.setVisibility(View.VISIBLE);
            Toast.makeText(getApplicationContext(), R.string.str_service_unavailable, Toast.LENGTH_SHORT)
                    .show();
        }
    };

    private Callback<PostResp> callback = new Callback<PostResp>() {

        @Override
        public void onResponse(Call<PostResp> call, Response<PostResp> res) {
            int code = res.code();
            if (code == 200) {
                postItemList = res.body().getPostItems();
                if (postItemList.isEmpty()){
                    text_empry_list.setVisibility(View.VISIBLE);
                }else {
                    text_empry_list.setVisibility(View.GONE);
                }
                initViews();
            } else if (code == 401) {
                ResponseBody base = res.errorBody();
                if (base != null) {
                    Toast.makeText(getApplicationContext(), base.toString(), Toast.LENGTH_SHORT).show();
                }

                finish();
            } else if (code == 500) {
                Toast.makeText(getApplicationContext(), R.string.str_unknown_error, Toast.LENGTH_SHORT)
                        .show();
            } else {
                Utilities.showMessage(R.string.str_zactu, R.string.str_service_unavailable, context);
            }
            progressbar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PostResp> call, Throwable t) {
            progressbar.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            tryAgain.setVisibility(View.VISIBLE);
            Toast.makeText(context, R.string.str_service_unavailable, Toast.LENGTH_SHORT)
                    .show();
        }
    };

    private void initViews() {
        recyclerView.setVisibility(View.VISIBLE);

        LinearLayoutManager mLayoutManager = new GridLayoutManager(this, Tools.calculateNoOfColumns(this));
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(mLayoutManager) {
            @Override
            public boolean onLoadMore(int current_page) {
                if (!hasPage) {
                    return false;
                }
                if (!Utilities.isInternetAvailable(getApplicationContext())) {
                    Toast.makeText(getApplicationContext(), R.string.str_no_internet, Toast.LENGTH_SHORT)
                            .show();
                    tryAgain.setVisibility(View.VISIBLE);
                    return false;
                }
                progressbar.setVisibility(View.VISIBLE);
                Call<PostResp> operations = postService
                        .postlist("", "", "", "", current_page + 1);
                operations.enqueue(page);
                return true;
            }
        });

        if (postItemList.size() == 10) {
            nextPage++;
            hasPage = true;
        } else {
            hasPage = false;
        }
        mAdapter = new RecipeGridAdapter(getApplicationContext(), postItemList);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter.notifyDataSetChanged();
        mAdapter.setOnItemClickListener(new RecipeGridAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(final View v, final News obj, int position) {
                App.nbrClicks++;
                if (mInterstitialAd.isLoaded() && App.nbrClicks > 3) {
                    showProgressDialog();
                    mInterstitialAd.show();
                    mInterstitialAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdClosed() {
                            super.onAdClosed();
                            App.nbrClicks = 0;
                            hideProgressDialog();
                            Intent myIntent = new Intent(ActivityBaseCategoryDetails.this, ActivityNewsDetails.class);
                            myIntent.putExtra("com.app.mobile.fashionable.EXTRA_OBJC", obj); //Optional parameters
                            ActivityBaseCategoryDetails.this.startActivity(myIntent);
                        }
                    });

                } else {
                    Intent myIntent = new Intent(ActivityBaseCategoryDetails.this, ActivityNewsDetails.class);
                    myIntent.putExtra("com.app.mobile.fashionable.EXTRA_OBJC", obj); //Optional parameters
                    ActivityBaseCategoryDetails.this.startActivity(myIntent);
                }
            }
        });
        swipeRefreshLayout.setRefreshing(false);
        progressbar.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        progressbar.setVisibility(View.GONE);
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage(getString(R.string.str_loading));
        }

        mProgressDialog.show();
    }

    public static void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
