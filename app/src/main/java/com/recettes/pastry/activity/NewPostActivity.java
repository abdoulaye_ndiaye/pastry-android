package com.recettes.pastry.activity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.recettes.pastry.BaseApp;
import com.recettes.pastry.R;
import com.recettes.pastry.model.Client;
import com.recettes.pastry.model.User;
import com.recettes.pastry.service.MyDownloadService;
import com.recettes.pastry.service.MyUploadService;
import com.recettes.pastry.tools.Utilities;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

/**
 * Activity to upload and download photos from Firebase Storage.
 *
 * See {@link MyUploadService} for upload example.
 * See {@link com.recettes.pastry.service.MyDownloadService} for download example.
 */

public class NewPostActivity extends BaseActivity {

    private static final String TAG = "NewPostActivity";
    private static final String REQUIRED = "Required";

    private static final int RC_TAKE_PICTURE = 101;
    private static final int RC_STORAGE_PERMS = 102;
    private static final int CAMERA_REQUEST = 1888;
    private static int RESULT_LOAD_IMAGE = 1;
    private static final int RC_SIGN_IN = 9001;

    private static final String KEY_FILE_URI = "key_file_uri";
    private static final String KEY_DOWNLOAD_URL = "key_download_url";
    public static final String EXTRA_DOWNLOAD_URL = "extra_download_url";

    private EditText mTitleField;
    private EditText mBodyField;
    private Button mIngredienButton;
    private FloatingActionButton mSubmitButton;
    private RadioGroup radioSexGroup;
    private RadioGroup radioCategoryGroup;
    private RadioButton radioSexButton;
    private RadioButton radioCategoryButton;
    private RadioButton radioFemale;
    private RadioButton radioMale;
    private TextView textviewSexe;
    private ImageView mImageView0, mImageView1, mImageView2, mImageView3, mImageView4, mImageView5;
    private Boolean isFabOpen = false;
    private FloatingActionButton fab,fab1,fab2;
    private Animation fab_open,fab_close,rotate_forward,rotate_backward;
    private BroadcastReceiver mBroadcastReceiver;
    private ProgressDialog mProgressDialog;
    private StorageReference mStorage;
    public static CallbackManager callbackManagerFacebook;

    private Uri mDownloadUrl = null;
    private Uri mFileUri = null;

    private FirebaseAuth mAuth;
    String picture0 = "", picture1 = "", picture2 = "", picture3 = "", picture4 = "", picture5 = "";
    int choixImage;
    Integer[] imageArray = { };
    private GoogleApiClient mGoogleApiClient;
    private LinearLayout signInGoogleButton;
    private ImageView mPasserField;
    private FirebaseAuth.AuthStateListener mAuthListener;
    Dialog dialog = null;
    String thisUserId;
    Uri param1 = null;
    Uri param2 = null;
    Uri param3 = null;
    Uri param4 = null;
    Uri param5 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_post);
        Toolbar mToolBar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
//        mToolBar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_arrow_left));
        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //What to do on back clicked
                finish();
            }
        });
        Intent intent = getIntent();
        mAuth = FirebaseAuth.getInstance();

        mStorage = FirebaseStorage.getInstance().getReference();
        // [END initialize_database_ref]

        if (savedInstanceState != null) {
            mFileUri = savedInstanceState.getParcelable(KEY_FILE_URI);
            mDownloadUrl = savedInstanceState.getParcelable(KEY_DOWNLOAD_URL);
        }

        mIngredienButton = findViewById(R.id.id_ingredient_button);
        mImageView0 = findViewById(R.id.imageView);
        mImageView1 = findViewById(R.id.imageView1);
        mImageView2 = findViewById(R.id.imageView2);
        mImageView3 = findViewById(R.id.imageView3);
        mImageView4 = findViewById(R.id.imageView4);
        mImageView5 = findViewById(R.id.imageView5);
        mIngredienButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                prepareGoToRecette();
            }
        });
        picture0 = intent.getStringExtra(EXTRA_DOWNLOAD_URL);

        if(getIntent().hasExtra(EXTRA_DOWNLOAD_URL)) {
            Bundle extras = getIntent().getExtras();
//            picture = intent.getStringExtra(EXTRA_DOWNLOAD_URL);
            Uri myUri = Uri.parse(extras.getString(EXTRA_DOWNLOAD_URL));
            picture0 = myUri.toString();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),myUri);
                mImageView0.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if(getIntent().hasExtra("camera_photo")) {
            Bundle extras = getIntent().getExtras();
            Uri myUri = Uri.parse(extras.getString("camera_photo"));
            picture0 = myUri.toString();
            Glide.with(getApplicationContext()).load(myUri)
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(new GlideDrawableImageViewTarget(mImageView0) {
                        @Override
                        public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
                            super.onResourceReady(resource, animation);
                        }

                        @Override
                        public void onLoadFailed(Exception e, Drawable errorDrawable) {
                            super.onLoadFailed(e, errorDrawable);
                            //never called
                        }
                    });
        }

        mImageView0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shoPopupImage(0, mImageView0);
            }
        });
        mImageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shoPopupImage(1, mImageView1);
            }
        });
        mImageView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shoPopupImage(2, mImageView2);
            }
        });
        mImageView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shoPopupImage(3, mImageView3);
            }
        });
        mImageView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shoPopupImage(4, mImageView4);
            }
        });
        mImageView5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shoPopupImage(5, mImageView5);
            }
        });
    }

    public void shoPopupImage(final int choix, ImageView mImageView){
        PopupMenu popup = new PopupMenu(NewPostActivity.this, mImageView);
        popup.getMenuInflater()
                .inflate(R.menu.options_menu, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.item_take_picture:
                        choixImage = choix;
                        launchCamera();
                        break;
                    case R.id.item_choose_picture:
                        choixImage = choix;
                        choosephoto();
                        break;
                }
                return true;
            }
        });

        popup.show();
    }

    public void prepareGoToRecette(){
        if (picture0 != null){
            if (!picture1.isEmpty()){
                param1 = Uri.parse(picture1);
            }
            if (!picture2.isEmpty()){
                param2 = Uri.parse(picture2);
            }
            if (!picture3.isEmpty()){
                param3 = Uri.parse(picture3);
            }
            if (!picture4.isEmpty()){
                param4 = Uri.parse(picture4);
            }
            if (!picture5.isEmpty()){
                param5 = Uri.parse(picture5);
            }

            Intent intent = new Intent(NewPostActivity.this, ActivityIngrediens.class);
            intent.putExtra("key", "");
            intent.putExtra(MyUploadService.EXTRA_FILE_URI, Uri.parse(picture0));
            intent.putExtra(MyUploadService.EXTRA_FILE_URI1, param1);
            intent.putExtra(MyUploadService.EXTRA_FILE_URI2, param2);
            intent.putExtra(MyUploadService.EXTRA_FILE_URI3, param3);
            intent.putExtra(MyUploadService.EXTRA_FILE_URI4, param4);
            intent.putExtra(MyUploadService.EXTRA_FILE_URI5, param5);
            NewPostActivity.this.startActivity(intent);
        }
    }

    @Override
    public void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    private void setEditingEnabled(boolean enabled) {
        mTitleField.setEnabled(enabled);
        mBodyField.setEnabled(enabled);
        if (enabled) {
            mSubmitButton.setVisibility(View.VISIBLE);
        } else {
            mSubmitButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle out) {
        String[] a={"haha"};
        out.putStringArray("MyStringarray", a);
        super.onSaveInstanceState(out);
        out.putParcelable(KEY_FILE_URI, mFileUri);
        out.putParcelable(KEY_DOWNLOAD_URL, mDownloadUrl);
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
        } else {
        }
        if (mDownloadUrl != null) {
        } else {
        }
    }

    public void choosephoto(){
        String perm = android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
        String perm1 = android.Manifest.permission.CAMERA;
        if (!EasyPermissions.hasPermissions(this, perm)) {
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_storage),
                    RC_STORAGE_PERMS, perm);
            return;
        }

        if (!EasyPermissions.hasPermissions(this, perm1)) {
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_storage),
                    RC_TAKE_PICTURE, perm1);
            return;
        }
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), RESULT_LOAD_IMAGE);
    }

    @AfterPermissionGranted(RC_STORAGE_PERMS)
    private void launchCamera() {
        Log.d(TAG, "launchCamera");
        String perm = android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
        String perm1 = Manifest.permission.CAMERA;
        if (!EasyPermissions.hasPermissions(this, perm)) {
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_storage),
                    RC_STORAGE_PERMS, perm);
            return;
        }
        if (!EasyPermissions.hasPermissions(this, perm1)) {
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_storage),
                    RC_TAKE_PICTURE, perm1);
            return;
        }
        File dir = new File(Environment.getExternalStorageDirectory() + "/photos");
        File file = new File(dir, UUID.randomUUID().toString() + ".jpg");
        try {
            if (!dir.exists()) {
                dir.mkdir();
            }
            boolean created = file.createNewFile();
            Log.d(TAG, "file.createNewFile:" + file.getAbsolutePath() + ":" + created);
        } catch (IOException e) {
            Log.e(TAG, "file.createNewFile" + file.getAbsolutePath() + ":FAILED", e);
        }
        mFileUri = FileProvider.getUriForFile(this, "com.app.mobile.cookisi.fileprovider", file);
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, mFileUri);
        List<ResolveInfo> resolveInfos = getPackageManager()
                .queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);
        for (ResolveInfo resolveInfo : resolveInfos) {
            String packageName = resolveInfo.activityInfo.packageName;
            grantUriPermission(packageName, mFileUri,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
        }
        startActivityForResult(takePictureIntent, RC_TAKE_PICTURE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult:" + requestCode + ":" + resultCode + ":" + data);
        if (requestCode == RC_TAKE_PICTURE) {
            if (resultCode == RESULT_OK) {
                if (mFileUri != null) {
                    Uri imageUri = CropImage.getPickImageResultUri(this, data);
                    switch (choixImage){
                        case 0:
                            setImage(mFileUri.toString(), mImageView1);
                            picture0 = imageUri.toString();
                            break;
                        case 1:
                            setImage(mFileUri.toString(), mImageView1);
                            picture1 = imageUri.toString();
                            break;
                        case 2:
                            setImage(mFileUri.toString(), mImageView2);
                            picture2 = imageUri.toString();
                            break;
                        case 3:
                            setImage(mFileUri.toString(), mImageView3);
                            picture3 = imageUri.toString();
                            break;
                        case 4:
                            setImage(mFileUri.toString(), mImageView4);
                            picture4 = imageUri.toString();
                            break;
                        case 5:
                            setImage(mFileUri.toString(), mImageView5);
                            picture5 = imageUri.toString();
                            break;
                    }
                } else {
                    Log.w(TAG, "File URI is null");
                }
            } else {
            }
        }

        if(resultCode != RESULT_CANCELED){
            if (requestCode == RESULT_LOAD_IMAGE) {
                Uri imageUri = CropImage.getPickImageResultUri(this, data);
                switch (choixImage){
                    case 0:
                        setImage(imageUri.toString(), mImageView0);
                        picture0 = imageUri.toString();
                        break;
                    case 1:
                        setImage(imageUri.toString(), mImageView1);
                        picture1 = imageUri.toString();
                        break;
                    case 2:
                        setImage(imageUri.toString(), mImageView2);
                        picture2 = imageUri.toString();
                        break;
                    case 3:
                        setImage(imageUri.toString(), mImageView3);
                        picture3 = imageUri.toString();
                        break;
                    case 4:
                        setImage(imageUri.toString(), mImageView4);
                        picture4 = imageUri.toString();
                        break;
                    case 5:
                        setImage(imageUri.toString(), mImageView5);
                        picture5 = imageUri.toString();
                        break;
                }
                Log.d(TAG, "uploadFromUri:src:" + imageUri.toString());
            }
        }
    }

    public void setImage(String url, ImageView imageView){
        Glide.with(getApplicationContext()).load(url)
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(new GlideDrawableImageViewTarget(imageView) {
                    @Override
                    public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
                        super.onResourceReady(resource, animation);
                    }

                    @Override
                    public void onLoadFailed(Exception e, Drawable errorDrawable) {
                        super.onLoadFailed(e, errorDrawable);
                        //never called
                    }
                });
    }

    private void dialogPeopoleDetails() {
        dialog = new Dialog(this);
        LoginButton loginButton;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_login);
        mAuth = FirebaseAuth.getInstance();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(this.getResources().getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        loginButton = (LoginButton) dialog.findViewById(R.id.button_facebook_login);
//        mEmailField = (EditText) dialog.findViewById(R.id.field_email);
//        mPasswordField = (EditText) dialog.findViewById(R.id.field_password);
        mPasserField = (ImageView) dialog.findViewById(R.id.closeLogin);
//        mSignInButton = (Button) dialog.findViewById(R.id.button_sign_in);
//        mSignUpButton = (Button) dialog.findViewById(R.id.button_sign_up);
        signInGoogleButton = (LinearLayout) dialog.findViewById(R.id.btn_compte_google);
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    if (user.getDisplayName() != null){
                        thisUserId = user.getUid();
                        onAuthSuccessKnownUser(user);
                    }
                } else {
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

        callbackManagerFacebook = CallbackManager.Factory.create();
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(callbackManagerFacebook, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                signInWithFacebook(loginResult.getAccessToken());
            }

            @Override

            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
            }
        });
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;

        dialog.show();
        dialog.getWindow().setAttributes(lp);
        signInGoogleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signInGoogle();
            }
        });
    }

    private void signInGoogle() {
        hideProgressDialog();
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        (this).startActivityForResult(signInIntent, RC_SIGN_IN);
        dialog.dismiss();
    }


    private void signInWithFacebook(AccessToken token) {
        hideProgressDialog();
        Log.d(TAG, "signInWithFacebook:" + token);
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(getApplicationContext(), "Echec connexion.",
                                    Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getApplicationContext(), "connexion réussie.",
                                    Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                            String uid=task.getResult().getUser().getUid();
                            String name=task.getResult().getUser().getDisplayName();
                            String email=task.getResult().getUser().getEmail();
                            String image=task.getResult().getUser().getPhotoUrl().toString();
                            final User userF = new User(name.toLowerCase(), email, image, FirebaseInstanceId.getInstance().getToken());
                            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                            Date date = new Date();
                            Client utilisateurC = new Client(name.toLowerCase(), email, dateFormat.format(date), Utilities.getUserCountry(getApplicationContext()));
//                            saveClient(utilisateurC);
                            BaseApp.authenticatedUser = userF;
                            if (getUid() != null){
                                FirebaseDatabase.getInstance().getReference("users").child(getUid()).addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        User user1 = dataSnapshot.getValue(User.class);
                                        if (user1 == null){
                                            FirebaseDatabase.getInstance().getReference("users").child(getUid()).setValue(userF);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                            }
                        }
                        hideProgressDialog();
                    }
                });
    }

    @Override
    public void onStart() {
        super.onStart();
        updateUI(mAuth.getCurrentUser());
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(this);
        manager.registerReceiver(mBroadcastReceiver, MyDownloadService.getIntentFilter());
        manager.registerReceiver(mBroadcastReceiver, MyUploadService.getIntentFilter());
    }

    @Override
    public void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    private void onAuthSuccessKnownUser(FirebaseUser user) {
        dialog.hide();
    }

}
