package com.recettes.pastry.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.recettes.pastry.BaseApp;
import com.recettes.pastry.R;
import com.recettes.pastry.controller.App;
import com.recettes.pastry.fragment.FragmentBeignets;
import com.recettes.pastry.fragment.FragmentBiscuits;
import com.recettes.pastry.fragment.FragmentCakes;
import com.recettes.pastry.fragment.FragmentLesRecettesdeBase;
import com.recettes.pastry.fragment.FragmentPainsetviennoiseries;
import com.recettes.pastry.fragment.FragmentTendances;
import com.recettes.pastry.fragment.FragmentVideos;
import com.recettes.pastry.model.UserCountryInfos;
import com.recettes.pastry.rest.CountryService;
import com.recettes.pastry.tools.MyNotificationOpenedHandler;
import com.recettes.pastry.tools.MyNotificationReceivedHandler;
import com.recettes.pastry.tools.Utilities;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.onesignal.OneSignal;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityMain extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar toolbar;
    private AdView mAdView;
    InterstitialAd mInterstitialAd;
    public static Activity mActivity;
    private NavigationView navigationView;
    private final Handler drawerHandler = new Handler();
    public static CallbackManager callbackManager;
    private FirebaseAuth mAuth;
    private ActionBar actionbar;
    CountryService countryService = BaseApp.mobileAdapter.create(CountryService.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mActivity = this;
        FacebookSdk.sdkInitialize(getApplicationContext());
        OneSignal.startInit(this)
                .setNotificationOpenedHandler(new MyNotificationOpenedHandler())
                .setNotificationReceivedHandler(new MyNotificationReceivedHandler())
                .init();
        OneSignal.setInFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification);

        MobileAds.initialize(this, getString(R.string.str_ad_id));
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.str_interstitial));
        requestNewInterstitial();
        setContentView(R.layout.activity_main);
        navigationView = findViewById(R.id.nav_view);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(false);
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        DatabaseReference tokenRef;
        tokenRef = FirebaseDatabase.getInstance().getReference();
        if (FirebaseAuth.getInstance().getCurrentUser() != null){
            String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
            tokenRef.child("tokens").child(uid).child("token").setValue(FirebaseInstanceId.getInstance().getToken());
        }
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        callbackManager = CallbackManager.Factory.create();

        setUpNavigationDrawer(getIntent().getIntExtra("menu", R.id.nav_tendances));

        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        getCountryInfos();
    }

    public void  getCountryInfos(){
        Call<UserCountryInfos> operations;
        operations = countryService.getInfos();
        operations.enqueue(callback);
    }

    private Callback<UserCountryInfos> callback = new Callback<UserCountryInfos>() {

        @Override
        public void onResponse(Call<UserCountryInfos> call, Response<UserCountryInfos> res) {
            int code = res.code();
            if (code == 200) {
                Utilities.setUserCountry(res.body().getCountry(), getApplicationContext());
            } else {
                Utilities.setUserCountry("Pays?", getApplicationContext());
            }
        }

        @Override
        public void onFailure(Call<UserCountryInfos> call, Throwable t) {

            Toast.makeText(getApplicationContext(), R.string.str_service_unavailable, Toast.LENGTH_SHORT)
                    .show();
        }
    };

    private void setUpNavigationDrawer(int menu) {
        View nav_header = LayoutInflater.from(this).inflate(R.layout.nav_header_main, null);
        navigationView.addHeaderView(nav_header);
        navigationView.inflateMenu(R.menu.activity_main_drawer);
        View headerView = navigationView.getHeaderView(0);
        TextView navUsername = headerView.findViewById(R.id.name_txtview);
        TextView emailtextview = headerView.findViewById(R.id.email_txtview);
        TextView connecttxtview = headerView.findViewById(R.id.connect_txtview);
        navigationView.setNavigationItemSelectedListener(this);
        navUsername.setText(null);
        emailtextview.setText(null);
        connecttxtview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ActivityLogin.class);
                startActivity(intent);
            }
        });

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();

        Fragment fragment = getFragment(menu);
        ft.replace(R.id.container, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();

        navigationView.getMenu().findItem(menu).setChecked(true);
    }

    @Nullable
    private Fragment getFragment(int id) {
        final Fragment[] fragment = new Fragment[1];
        switch (id) {
            case R.id.nav_tendances:
                fragment[0] = new FragmentTendances();
                getSupportActionBar().setTitle("Home");
                break;
            case R.id.nav_cake:
                fragment[0] = new FragmentCakes();
                getSupportActionBar().setTitle("Cakes");
                break;
            case R.id.nav_pains_viennoiseries:
                fragment[0] = new FragmentPainsetviennoiseries();
                getSupportActionBar().setTitle("Breads and pastries");
                break;
            case R.id.nav_biscuits:
                fragment[0] = new FragmentBiscuits();
                getSupportActionBar().setTitle("Biscuits");
                break;
            case R.id.nav_recettes_de_bases:
                fragment[0] = new FragmentLesRecettesdeBase();
                getSupportActionBar().setTitle("Basic recipes");
                break;
            case R.id.nav_beignets:
                fragment[0] = new FragmentBeignets();
                getSupportActionBar().setTitle("Doughnuts");
                break;
            case R.id.nav_videos:
                fragment[0] = new FragmentVideos();
                getSupportActionBar().setTitle("Videos");
                break;
            case R.id.nav_shop:
                Intent intentShop = new Intent(getApplicationContext(), ActivityShopList.class);
                startActivity(intentShop);
                break;
            case R.id.nav_favoris:
                if (mInterstitialAd.isLoaded()){
                    showProgressDialog();
                    mInterstitialAd.show();
                    mInterstitialAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdClosed() {
                            super.onAdClosed();
                            App.nbrClicks = 0;
                            hideProgressDialog();
                            Intent intent = new Intent(getApplicationContext(), ActivityFavori.class);
                            startActivity(intent);
                        }
                    });
                }else {
                    Intent intent = new Intent(getApplicationContext(), ActivityFavori.class);
                    startActivity(intent);
                }
                break;
            case R.id.nav_param:
                Intent intent = new Intent(getApplicationContext(), ActivityParametres.class);
                startActivity(intent);
                break;
            default:
                fragment[0] = null;
        }
        return fragment[0];
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice(AdRequest.DEVICE_ID_EMULATOR)
                .build();
        mInterstitialAd.loadAd(adRequest);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        final Fragment fragment;
        FragmentManager fragmentManager = getSupportFragmentManager();
        final FragmentTransaction ft = fragmentManager.beginTransaction();
        int id = item.getItemId();

        fragment = getFragment(id);

        drawerHandler.removeCallbacksAndMessages(null);
        drawerHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (fragment == null) {
                    return;
                }
                ft.replace(R.id.container, fragment);
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                ft.commit();
            }
        }, 250);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_param_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        FragmentManager fragmentManager = getSupportFragmentManager();
        final FragmentTransaction ft = fragmentManager.beginTransaction();
        switch (item.getItemId()) {
            case R.id.action_favori:
                if (mInterstitialAd.isLoaded()){
                    showProgressDialog();
                    mInterstitialAd.show();
                    mInterstitialAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdClosed() {
                            super.onAdClosed();
                            App.nbrClicks = 0;
                            hideProgressDialog();
                            Intent intent = new Intent(getApplicationContext(), ActivityFavori.class);
                            startActivity(intent);
                        }
                    });
                }else {
                    Intent intent = new Intent(getApplicationContext(), ActivityFavori.class);
                    startActivity(intent);
                }
                return true;
            case R.id.action_settings:
                Intent intent = new Intent(getApplicationContext(), ActivityParametres.class);
                startActivity(intent);
                return true;
            case R.id.action_shop_list:
                Intent intentShop = new Intent(getApplicationContext(), ActivityShopList.class);
                startActivity(intentShop);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        requestNewInterstitial();
        super.onResume();
    }

    @Override
    public void onPause() {
        requestNewInterstitial();
        super.onPause();
    }

}
