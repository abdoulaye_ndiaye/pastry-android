package com.recettes.pastry.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.recettes.pastry.BaseApp;
import com.recettes.pastry.R;
import com.recettes.pastry.adapter.AdapterIngredientList;
import com.recettes.pastry.adapter.AdapterList;
import com.recettes.pastry.adapter.AdapterNewsListWithoutHeader;
import com.recettes.pastry.adapter.SliderAdapter;
import com.recettes.pastry.controller.App;
import com.recettes.pastry.model.News;
import com.recettes.pastry.model.NewsResp;
import com.recettes.pastry.model.PhotoModel;
import com.recettes.pastry.model.PostResp;
import com.recettes.pastry.model.ResponseBase;
import com.recettes.pastry.model.Client;
import com.recettes.pastry.rest.PostService;
import com.recettes.pastry.rest.UserService;
import com.recettes.pastry.tools.Utilities;
import com.recettes.pastry.view.EndlessRecyclerOnScrollListener;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import me.relex.circleindicator.CircleIndicator;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityNewsDetails extends BaseActivity implements BaseSliderView.OnSliderClickListener, ViewPagerEx.OnPageChangeListener {
    public static final String EXTRA_OBJC = "com.app.mobile.fashionable.EXTRA_OBJC";
    public static void navigate(AppCompatActivity activity, View transitionImage, News obj) {
        Intent intent = new Intent(activity, ActivityNewsDetails.class);
        intent.putExtra(EXTRA_OBJC, obj);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitionImage, EXTRA_OBJC);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }

    private Toolbar toolbar;
    private ActionBar actionBar;
    private View parent_view;
    private FloatingActionButton fab;
    public ImageView bt_share0;
    public ImageView bt_share1;
    public ImageView bt_share2;
    public ImageView bt_share3;
    private RecyclerView recyclerViewSame;
    private RecyclerView recyclerIngredients;
    private RecyclerView recyclerInstructions;
    private ProgressBar progressbar;
    PostService postService = BaseApp.mobileAdapter.create(PostService.class);
    UserService userService = BaseApp.mobileAdapter.create(UserService.class);
    private Button btn_send;
    private EditText et_content;
    private News news;
    Dialog dialog = null;
    private FirebaseAuth mAuth;
    String thisUserId;
    private static final int RC_SIGN_IN = 9001;
    private static ViewPager mPager;
    private static int currentPage = 0;
    public DatabaseReference globalPostRef = null;
    public boolean exists = false;
    private int nextPage = 1;
    private boolean hasPage = true;
    public static AdapterNewsListWithoutHeader mAdapter;
    public static AdapterIngredientList mAdapterIngredients;
    public static AdapterList mAdapterInstructions;
    List<News> postItemList;
    InterstitialAd mInterstitialAd;
    private AdView mAdView;
    private AdView mAdView1;
    public WebView videoView;
    public CardView cardvideoView;
    public static boolean HIDE_TOOLBAR_OLD_DEVICES = true;
    private boolean systemUIVisible;
    TextView prepaTextView;
    TextView personTextView;
    TextView dateTextView;
    TextView channelTextView;
    TextView durationTotalTextView;
    TextView cuissonTextView;
    LinearLayout linearPrepa;
    LinearLayout linearPerson;
    LinearLayout linearTotal;
    LinearLayout linearCuisson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_details);
        parent_view = findViewById(android.R.id.content);
        news = (News) getIntent().getSerializableExtra(EXTRA_OBJC);
        initToolbar();
        fab = findViewById(R.id.fab);
        cardvideoView = findViewById(R.id.card_webview);
        videoView = findViewById(R.id.youtubePlayerView);
        bt_share0 = findViewById(R.id.bt_share0);
        bt_share1 = findViewById(R.id.bt_share1);
        bt_share2 = findViewById(R.id.bt_share2);
        bt_share3 = findViewById(R.id.bt_share3);
        linearPrepa = findViewById(R.id.linearPrepa);
        linearCuisson = findViewById(R.id.linearCuisson);
        linearTotal = findViewById(R.id.linearTotal);
        linearPerson = findViewById(R.id.linearPerson);
        progressbar = findViewById(R.id.progressbar);
        recyclerViewSame = findViewById(R.id.recyclerViewSame);
        recyclerInstructions = findViewById(R.id.recyclerInstructions);
        recyclerIngredients = findViewById(R.id.recyclerIngredients);
        prepaTextView = findViewById(R.id.duration_prepa);
        personTextView = findViewById(R.id.person_prepa);
        dateTextView = findViewById(R.id.date);
        channelTextView = findViewById(R.id.channel);
        durationTotalTextView = findViewById(R.id.duration_total);
        cuissonTextView = findViewById(R.id.duration_cuisson);
        MobileAds.initialize(this, getString(R.string.str_ad_id));
        mInterstitialAd = new InterstitialAd(getApplicationContext());
        mInterstitialAd.setAdUnitId(getString(R.string.str_interstitial));
        requestNewInterstitial();

        getNewsInfos(news.getId());
        getclientInfos(news.getCategory());

        iniComponen();
        FirebaseApp.initializeApp(this);
        mAdView = findViewById(R.id.adView);
        mAdView1 = findViewById(R.id.adView1);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mAdView1.loadAd(adRequest);
        init();
        if (getUid() != null){
            final DatabaseReference favoriteRef = FirebaseDatabase.getInstance().getReference("favorit").child(getUid());
            favoriteRef.child("idposts").addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot != null) {
                        for (DataSnapshot children : dataSnapshot.getChildren()) {
                            String idpost = children.getValue(String.class);
                            assert idpost != null;
                            if (idpost.equals(news.getId())){
                                fab.setImageResource(R.drawable.ic_heart_plein_white);
                                exists = true;
                            }
                        }
                        if (!exists){
                            fab.setImageResource(R.drawable.ic_heart_white);
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getUid() == null){
                    Intent intent = new Intent(getApplicationContext(), ActivityLogin.class);
                    startActivity(intent);
                }else {
                    final DatabaseReference favoriteRef = FirebaseDatabase.getInstance().getReference("favorit").child(getUid());
                    favoriteRef.child("idposts").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot != null) {
                                for (DataSnapshot children : dataSnapshot.getChildren()) {
                                    String idpost = children.getValue(String.class);
                                    if (idpost.equals(news.getId())){
                                        favoriteRef.child("idposts").child(news.getId()).removeValue();
                                        favoriteRef.child("posts").child(news.getId()).removeValue();
                                        fab.setImageResource(R.drawable.ic_heart_white);
                                        Snackbar.make(parent_view, "Removed from your favorites", Snackbar.LENGTH_LONG).show();
                                        exists = true;
                                    }
                                }
                                if (!exists){
                                    Map<String, Object> element= new HashMap<>();
                                    Map<String, Object> data= new HashMap<>();
                                    element.put(news.getId(),news.getId());
                                    data.put(news.getId(), news);
                                    favoriteRef.child("idposts").updateChildren(element);
                                    favoriteRef.child("posts").updateChildren(data);
                                    fab.setImageResource(R.drawable.ic_heart_plein_white);
                                    Snackbar.make(parent_view, "Added to your favorites", Snackbar.LENGTH_LONG).show();
                                }
                                exists = false;
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    }) ;
                }
            }
        });
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle("");
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("SEE_YOUR_LOGCAT_TO_GET_YOUR_DEVICE_ID")
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

    private void init() {
        mPager = findViewById(R.id.pager);
        mPager.setAdapter(new SliderAdapter(getApplicationContext() ,news.getUrls()));
        CircleIndicator indicator = findViewById(R.id.indicator);
        indicator.setViewPager(mPager);

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == news.getUrls().size()) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        if (getUid() != null){
            final DatabaseReference favoriteRef = FirebaseDatabase.getInstance().getReference("favorit").child(getUid());
            favoriteRef.child("idposts").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot != null) {
                        for (DataSnapshot children : dataSnapshot.getChildren()) {
                            String idpost = children.getValue(String.class);
                            if (idpost.equals(news.getId())){
                                fab.setImageResource(R.drawable.ic_heart_plein_white);
                                exists = true;
                            }
                        }
                        if (!exists){
                            fab.setImageResource(R.drawable.ic_heart_white);
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    private void initViews() {
        recyclerViewSame.setVisibility(View.VISIBLE);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerInstructions.setLayoutManager(linearLayoutManager);

        LinearLayoutManager linearLayoutManagerIng = new LinearLayoutManager(this);
        linearLayoutManagerIng.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerIngredients.setLayoutManager(linearLayoutManagerIng);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        layoutManager.scrollToPosition(0);
        recyclerViewSame.setLayoutManager(layoutManager);

        recyclerViewSame.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public boolean onLoadMore(int current_page) {
                if (!hasPage) {
                    return false;
                }
                if (!Utilities.isInternetAvailable(ActivityNewsDetails.this)) {
                    Toast.makeText(ActivityNewsDetails.this, R.string.str_no_internet, Toast.LENGTH_SHORT)
                            .show();
                    return false;
                }
                progressbar.setVisibility(View.VISIBLE);
                Call<PostResp> operations = postService
                        .postlist("", "", "", "", current_page + 1);
                operations.enqueue(page);
                return true;
            }
        });

        if (postItemList.size() == 10) {
            nextPage++;
            hasPage = true;
        } else {
            hasPage = false;
        }
        if (postItemList.size() > 0){
            mAdapter = new AdapterNewsListWithoutHeader(ActivityNewsDetails.this, postItemList.get(0), postItemList);
        }else {
            mAdapter = new AdapterNewsListWithoutHeader(ActivityNewsDetails.this, null, postItemList);
        }
        recyclerViewSame.setAdapter(mAdapter);
        recyclerViewSame.setItemAnimator(new DefaultItemAnimator());
        mAdapter.notifyDataSetChanged();

        mAdapter.setOnItemClickListener(new AdapterNewsListWithoutHeader.OnItemClickListener() {
            @Override
            public void onItemClick(final View v, final News obj, int position) {
                App.nbrClicks++;
                if (App.nbrClicks > 3 && mInterstitialAd.isLoaded()){
                    mInterstitialAd.show();
                    mInterstitialAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdClosed() {
                            super.onAdClosed();
                            hideProgressDialog();
                            ActivityNewsDetails.navigate(ActivityNewsDetails.this, v.findViewById(R.id.image), obj);
                            finish();
                        }
                    });
                }else {
                    ActivityNewsDetails.navigate(ActivityNewsDetails.this, v.findViewById(R.id.image), obj);
                    finish();
                }

            }
        });
        progressbar.setVisibility(View.GONE);
    }

    private void getclientInfos(String category) {
        if (!Utilities.isInternetAvailable(ActivityNewsDetails.this)) {
            progressbar.setVisibility(View.GONE);
            Toast.makeText(this,getString(R.string.no_internet) + "",Toast.LENGTH_SHORT).show();
            return;
        }
        progressbar.setVisibility(View.VISIBLE);
        Call<PostResp> operations;
        operations = postService.postlist("", category, "", "", nextPage);
        operations.enqueue(callbackSame);
    }

    private Callback<PostResp> callbackSame = new Callback<PostResp>() {

        @Override
        public void onResponse(Call<PostResp> call, Response<PostResp> res) {
            int code = res.code();
            if (code == 200) {
                postItemList = res.body().getPostItems();
                if (postItemList.isEmpty()){
//                    text_empry_list.setVisibility(View.VISIBLE);
                }else {
//                    text_empry_list.setVisibility(View.GONE);
                }
                initViews();
            } else if (code == 401) {
                ResponseBody base = res.errorBody();
                if (base != null) {
                    Toast.makeText(ActivityNewsDetails.this, base.toString(), Toast.LENGTH_SHORT).show();
                }

                finish();
            } else if (code == 500) {
                Toast.makeText(ActivityNewsDetails.this, R.string.str_unknown_error, Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(getApplicationContext(),R.string.str_service_unavailable + "",Toast.LENGTH_SHORT).show();
            }
            progressbar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PostResp> call, Throwable t) {
            progressbar.setVisibility(View.GONE);
//            swipeRefreshLa2yout.setRefreshing(false);
//            tryAgain.setVisibility(View.VISIBLE);
            Toast.makeText(ActivityNewsDetails.this, R.string.str_service_unavailable, Toast.LENGTH_SHORT)
                    .show();
        }
    };

    private Callback<PostResp> page = new Callback<PostResp>() {
        @Override
        public void onResponse(Call<PostResp> call, Response<PostResp> res) {
            progressbar.setVisibility(View.VISIBLE);
            int code = res.code();
            if (code == 200) {
                postItemList.addAll(res.body().getPostItems());
                mAdapter.notifyDataSetChanged();

                progressbar.setVisibility(View.GONE);
            }
            if (code == 401) {
                ResponseBody base = res.errorBody();
                if (base != null) {
                    //TODO Client inconnu. Redirect to login screen
                    Toast.makeText(ActivityNewsDetails.this, res.message(), Toast.LENGTH_SHORT)
                            .show();
                }
                finish();
            }
            if (code == 500) {
                Toast.makeText(ActivityNewsDetails.this, R.string.str_unknown_error, Toast.LENGTH_SHORT)
                        .show();
            }
            progressbar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PostResp> call, Throwable t) {
            progressbar.setVisibility(View.GONE);
            Toast.makeText(ActivityNewsDetails.this, R.string.str_service_unavailable, Toast.LENGTH_SHORT)
                    .show();
        }
    };

    private Callback<NewsResp> callback = new Callback<NewsResp>() {

        @Override
        public void onResponse(Call<NewsResp> call, Response<NewsResp> res) {
            int code = res.code();
            if (code == 200) {
                initViews(res.body().getPost());
            } else if (code == 401) {
                ResponseBody base = res.errorBody();
                if (base != null) {
                    Toast.makeText(getApplicationContext(), base.toString(), Toast.LENGTH_SHORT).show();
                }

            } else if (code == 500) {
                Toast.makeText(getApplicationContext(), R.string.str_unknown_error, Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(getApplicationContext(), R.string.str_service_unavailable, Toast.LENGTH_SHORT)
                        .show();
            }
            progressbar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<NewsResp> call, Throwable t) {
            progressbar.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), R.string.str_service_unavailable, Toast.LENGTH_SHORT)
                    .show();
        }
    };

    private Callback<ResponseBase> callbackUser = new Callback<ResponseBase>() {

        @Override
        public void onResponse(Call<ResponseBase> call, Response<ResponseBase> res) {
            int code = res.code();
            if (code == 200) {
                Toast.makeText(getApplicationContext(), "Email address saved", Toast.LENGTH_SHORT)
                        .show();
                et_content.setText("");
            } else if (code == 401) {
                ResponseBody base = res.errorBody();
                if (base != null) {
                    Toast.makeText(getApplicationContext(), base.toString(), Toast.LENGTH_SHORT).show();
                }

            } else if (code == 500) {
                Toast.makeText(getApplicationContext(), R.string.str_unknown_error, Toast.LENGTH_SHORT)
                        .show();
            } else {
                Toast.makeText(getApplicationContext(), R.string.str_service_unavailable, Toast.LENGTH_SHORT)
                        .show();
            }
            progressbar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<ResponseBase> call, Throwable t) {
            progressbar.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), R.string.str_service_unavailable, Toast.LENGTH_SHORT)
                    .show();
        }
    };

    private void getNewsInfos(String postid) {
        if (!Utilities.isInternetAvailable(getApplicationContext())) {
            progressbar.setVisibility(View.GONE);
            Toast.makeText(this,getString(R.string.no_internet) + "",Toast.LENGTH_SHORT).show();
            return;
        }
        progressbar.setVisibility(View.VISIBLE);
        Call<NewsResp> operations;
        operations = postService.post(postid);
        operations.enqueue(callback);
    }

    private void saveClient(Client utilisateur) {
        if (!Utilities.isInternetAvailable(getApplicationContext())) {
            progressbar.setVisibility(View.GONE);
            Toast.makeText(this,getString(R.string.no_internet) + "",Toast.LENGTH_SHORT).show();
            return;
        }
        progressbar.setVisibility(View.VISIBLE);
        Call<ResponseBase> operations;
        operations = userService.postUser(utilisateur);
        operations.enqueue(callbackUser);
    }

    private void initViews(final News news) {
        if (news.getPrepa()!= null && !news.getPrepa().isEmpty()){
            linearPrepa.setVisibility(View.VISIBLE);
            prepaTextView.setText(news.getPrepa());
        }
        if (news.getPerson()!= null && !news.getPerson().isEmpty()){
            linearPerson.setVisibility(View.VISIBLE);
            personTextView.setText(news.getPerson());
        }
        if (news.getCuisson()!= null && !news.getCuisson().isEmpty()){
            linearTotal.setVisibility(View.VISIBLE);
            durationTotalTextView.setText(news.getCuisson());
        }
        if (news.getTotaltime()!= null && !news.getTotaltime().isEmpty()){
            linearCuisson.setVisibility(View.VISIBLE);
            cuissonTextView.setText(news.getTotaltime());
        }
        if (news.getPosttitre()!= null && !news.getPosttitre().isEmpty()){
            channelTextView.setVisibility(View.VISIBLE);
            channelTextView.setText(news.getPosttitre());
        }

        if (news.getVideo()!= null && !news.getVideo().isEmpty()){
            cardvideoView.setVisibility(View.VISIBLE);

            String frameVideo = "<iframe width='100%' height='280' src=\"https://www.youtube.com/embed/"+news.getVideo()+"?autoplay=0\" frameborder=\"0\" allowfullscreen=\"allowfullscreen\"></iframe>";

            videoView.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    return false;
                }
            });

            WebSettings webSettings = videoView.getSettings();
            webSettings.setJavaScriptEnabled(true);
            videoView.loadData(frameVideo, "text/html", "utf-8");

            videoView.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    if (systemUIVisible)
                        hideSystemUI();
                    else
                        showSystemUI();
                }
            });
        }

        if (news.getIngredients() != null){
            List<String> items = Arrays.asList(news.getIngredients().split("\\s*,\\s*"));
            mAdapterIngredients =  new AdapterIngredientList(ActivityNewsDetails.this, items, news);
            recyclerIngredients.setAdapter(mAdapterIngredients);
            mAdapterIngredients.notifyDataSetChanged();
        }
        if (news.getPosttexte() != null){
            List<String> items = Arrays.asList(news.getPosttexte().split("\\s*;\\s*"));
            mAdapterInstructions =  new AdapterList(ActivityNewsDetails.this, items);
            recyclerInstructions.setAdapter(mAdapterInstructions);
            mAdapterInstructions.notifyDataSetChanged();
        }
        for(PhotoModel name : news.getUrls()){
            TextSliderView textSliderView = new TextSliderView(this);
            textSliderView
                    .image(name.getUrl())
                    .setScaleType(BaseSliderView.ScaleType.FitCenterCrop)
                    .setOnSliderClickListener(this);

            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra",name.getUrl());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
            return true;
        }
        if (item.getItemId() == R.id.action_share){
            if (news.getShare() != null && !news.getShare().isEmpty()){
                sharelink(news.getShare());
            }else {
                sharelink("https://play.google.com/store/apps/details?id=com.recettes.pastry");
            }
        }
        return super.onOptionsItemSelected(item);
    }

    public void sharelink(String link){
        String title = "Pastry"; //Title you wants to share
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, title);
        shareIntent.setType("text/plain");
        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        shareIntent.putExtra(Intent.EXTRA_TEXT, link);
        startActivity(Intent.createChooser(shareIntent, news.getTitle()));
    }

    @Override
    protected void onStop() {
//        imageSlider.stopAutoCycle();
        super.onStop();
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        Toast.makeText(this,slider.getBundle().get("extra") + "",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

    @Override
    public void onPageSelected(int position) {
        Log.e("Slider Demo", "Page Changed: " + position);
    }

    @Override
    public void onPageScrollStateChanged(int state) {}

    public void iniComponen() {
        btn_send = findViewById(R.id.btn_send);
        et_content = findViewById(R.id.text_content_address);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                postComment(et_content.getText().toString());
                et_content.setText("");
                hideKeyboard();
            }
        });
        et_content.addTextChangedListener(contentWatcher);
        if (et_content.length() == 0) {
            btn_send.setEnabled(false);
        }
    }

    private void hideKeyboard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private TextWatcher contentWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable etd) {
            if (etd.toString().trim().length() == 0) {
                btn_send.setEnabled(false);
            } else {
                btn_send.setEnabled(true);
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }
    };

    private void postComment(final String content) {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yy");
        String formattedDate = df.format(c.getTime());
        Client client = new Client("", content, formattedDate, Utilities.getUserCountry(this));
        saveClient(client);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_favori_main, menu);
        return true;
    }

    @SuppressLint("InlinedApi")
    private void showSystemUI(){
        // getSupportActionBar().show();
        if (android.os.Build.VERSION.SDK_INT >= 19)
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
        systemUIVisible = true;
    }

    @SuppressLint("InlinedApi")
    private void hideSystemUI(){

        if (getSupportActionBar() != null){
            if (!(HIDE_TOOLBAR_OLD_DEVICES && android.os.Build.VERSION.SDK_INT <= 19)) {
                //getSupportActionBar().hide();
            }
        }

        if (android.os.Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE |
                            View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION |
                            View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN |
                            View.SYSTEM_UI_FLAG_HIDE_NAVIGATION |
                            View.SYSTEM_UI_FLAG_FULLSCREEN |
                            View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        }

        systemUIVisible = false;
    }

}
