package com.recettes.pastry.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.recettes.pastry.BaseApp;
import com.recettes.pastry.R;
import com.recettes.pastry.adapter.AdapterNewsListWithHeader;
import com.recettes.pastry.data.Constant;
import com.recettes.pastry.data.GlobalVariable;
import com.recettes.pastry.data.Tools;
import com.recettes.pastry.model.Channel;
import com.recettes.pastry.model.News;
import com.recettes.pastry.model.PostResp;
import com.recettes.pastry.rest.PostService;
import com.recettes.pastry.tools.Utilities;
import com.recettes.pastry.view.EndlessRecyclerOnScrollListener;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityChannelDetails extends AppCompatActivity {
    public static final String EXTRA_OBJCT = "com.app.sample.news.CHANNEL";
    private ProgressBar progressbar;
    List<News> postItemList;
    TextView text_empry_list;
    ImageButton tryAgain;
    private RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefreshLayout;
    private int nextPage = 1;
    PostService postService = BaseApp.mobileAdapter.create(PostService.class);
    Context context;
    private boolean hasPage = true;

    // give preparation animation activity transition
    public static void navigate(AppCompatActivity activity, View transitionImage, Channel obj) {
        Intent intent = new Intent(activity, ActivityChannelDetails.class);
        intent.putExtra(EXTRA_OBJCT, obj);
        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitionImage, EXTRA_OBJCT);
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }

    private Toolbar toolbar;
    private ActionBar actionBar;
    private AdapterNewsListWithHeader mAdapter;
    private Channel channel;
    private GlobalVariable global;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_channel_details);
        context = getApplicationContext();

        // animation transition
        ViewCompat.setTransitionName(findViewById(android.R.id.content), EXTRA_OBJCT);

        // get extra object
        channel = (Channel) getIntent().getSerializableExtra(EXTRA_OBJCT);


        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        text_empry_list = (TextView) findViewById(R.id.text_empry_list);
        tryAgain = (ImageButton) findViewById(R.id.try_again);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);

        initToolbar();
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                nextPage = 1;
                getclientInfos(channel.getName());
            }
        });
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        String chnnl = channel.getName();
        List<Channel> channels = Constant.getChannelData(this);
        List<News>  list_news  = new ArrayList<>();
//        global = (GlobalVariable) getApplication();

        if(chnnl.equalsIgnoreCase(channels.get(0).getName())){       // politics
            list_news =  postItemList;

        }else if(chnnl.equalsIgnoreCase(channels.get(1).getName())){ // Entertainment
            list_news =  postItemList;

        }else if(chnnl.equalsIgnoreCase(channels.get(2).getName())){ // Science
            list_news =  postItemList;

        }else if(chnnl.equalsIgnoreCase(channels.get(3).getName())){ // Sport
            list_news =  postItemList;

        }else if(chnnl.equalsIgnoreCase(channels.get(4).getName())){ // Business
            list_news =  postItemList;

        }else if(chnnl.equalsIgnoreCase(channels.get(5).getName())){ // Technology
            list_news =  postItemList;
        }

        //set data and list adapter
//        mAdapter = new AdapterNewsListWithHeader(this, list_news.get(list_news.size()-1), list_news);
//        recyclerView.setAdapter(mAdapter);

//        mAdapter.setOnItemClickListener(new AdapterNewsListWithHeader.OnItemClickListener() {
//            @Override
//            public void onItemClick(View v, News obj, int position) {
//                ActivityNewsDetails.navigate(ActivityChannelDetails.this, v.findViewById(R.id.image), obj);
//            }
//        });

        // for system bar in lollipop
//        Tools.systemBarLolipop(this);
    }

    private void getclientInfos(String category) {
        if (!Utilities.isInternetAvailable(this)) {
            progressbar.setVisibility(View.GONE);
            Utilities.showMessage(null, getString(R.string.no_internet), context);
            tryAgain.setVisibility(View.VISIBLE);
            return;
        }
        progressbar.setVisibility(View.VISIBLE);
        Call<PostResp> operations;
        operations = postService.postlist("", category, "", "", nextPage);
        operations.enqueue(callback);
    }

    private Callback<PostResp> page = new Callback<PostResp>() {
        @Override
        public void onResponse(Call<PostResp> call, Response<PostResp> res) {
            progressbar.setVisibility(View.VISIBLE);
            int code = res.code();
            if (code == 200) {
                postItemList.addAll(res.body().getPostItems());
                mAdapter.notifyDataSetChanged();

                progressbar.setVisibility(View.GONE);
            }
            if (code == 401) {
                ResponseBody base = res.errorBody();
                if (base != null) {
                    //TODO Client inconnu. Redirect to login screen
                    Toast.makeText(context, res.message(), Toast.LENGTH_SHORT)
                            .show();
                }
//                getActivity().finish();
            }
            if (code == 500) {
                Toast.makeText(context, R.string.str_unknown_error, Toast.LENGTH_SHORT)
                        .show();
            }
            progressbar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PostResp> call, Throwable t) {
            progressbar.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            tryAgain.setVisibility(View.VISIBLE);
            Toast.makeText(context, R.string.str_service_unavailable, Toast.LENGTH_SHORT)
                    .show();
        }
    };

    private Callback<PostResp> callback = new Callback<PostResp>() {

        @Override
        public void onResponse(Call<PostResp> call, Response<PostResp> res) {
            int code = res.code();
            if (code == 200) {
                postItemList = res.body().getPostItems();
                if (postItemList.isEmpty()){
                    text_empry_list.setVisibility(View.VISIBLE);
                }
                initViews();
            } else if (code == 401) {
                ResponseBody base = res.errorBody();
                if (base != null) {
                    Toast.makeText(context, base.toString(), Toast.LENGTH_SHORT).show();
                }

//                getActivity().finish();
            } else if (code == 500) {
                Toast.makeText(context, R.string.str_unknown_error, Toast.LENGTH_SHORT)
                        .show();
            } else {
                Utilities.showMessage(R.string.str_zactu, R.string.str_service_unavailable, context);
            }
            progressbar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PostResp> call, Throwable t) {
            progressbar.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            tryAgain.setVisibility(View.VISIBLE);
            Toast.makeText(context, R.string.str_service_unavailable, Toast.LENGTH_SHORT)
                    .show();
        }
    };

    private void initViews() {
        recyclerView.setVisibility(View.VISIBLE);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.scrollToPosition(0);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public boolean onLoadMore(int current_page) {
                if (!hasPage) {
                    return false;
                }
                if (!Utilities.isInternetAvailable(context)) {
                    Toast.makeText(context, R.string.str_no_internet, Toast.LENGTH_SHORT)
                            .show();
                    tryAgain.setVisibility(View.VISIBLE);
                    return false;
                }
                progressbar.setVisibility(View.VISIBLE);
                Call<PostResp> operations = postService
                        .postlist("", "", "", "", current_page + 1);
                operations.enqueue(page);
                return true;
            }
        });

        if (postItemList.size() == 10) {
            nextPage++;
            hasPage = true;
        } else {
            hasPage = false;
        }
        if (postItemList.size() > 0){
            mAdapter = new AdapterNewsListWithHeader(this, postItemList.get(postItemList.size()-1), postItemList);
        }else {
            mAdapter = new AdapterNewsListWithHeader(this, null, postItemList);
        }
        recyclerView.setAdapter(mAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter.notifyDataSetChanged();
        mAdapter.setOnItemClickListener(new AdapterNewsListWithHeader.OnItemClickListener() {
            @Override
            public void onItemClick(View v, News obj, int position) {
                ActivityNewsDetails.navigate(ActivityChannelDetails.this, v.findViewById(R.id.image), obj);
            }
        });
//        mAdapter.setOnItemClickListener(new AdapterNewsList.OnItemClickListener() {
////            @Override
////            public void onItemClick(final View v, final PostItem obj, int position) {
////                randomNumber = random.nextInt(10) + 1;
////                if (mInterstitialAd.isLoaded() && randomNumber > 3) {
////                    showProgressDialog();
////                    mInterstitialAd.show();
////                    mInterstitialAd.setAdListener(new AdListener() {
////                        @Override
////                        public void onAdClosed() {
////                            super.onAdClosed();
////                            hideProgressDialog();
////                            onPhotolicked(obj);
////                        }
////                    });
////
////                } else {
////                    onPhotolicked(obj);
////                }
////            }
//
//            @Override
//            public void onItemClick(View view, News obj, int position) {
//
//            }
//        });
        swipeRefreshLayout.setRefreshing(false);
        progressbar.setVisibility(View.GONE);
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setTitle(channel.getName());
        getclientInfos(channel.getName());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
