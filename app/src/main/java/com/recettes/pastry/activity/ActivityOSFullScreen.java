package com.recettes.pastry.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.recettes.pastry.R;
import com.recettes.pastry.adapter.SliderAdapter;
import com.recettes.pastry.model.PhotoModel;
import com.recettes.pastry.model.VenteModel;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;
import uk.co.senab.photoview.PhotoViewAttacher;


/**
 * Created by ghambyte on 30/04/2017 at 21:05.
 * Project name : Modelic-V2
 */

public class ActivityOSFullScreen extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener{

    public static final String EXTRA_POST_ITEM = "com.modelic.android.model.PostItem";
    public static final String KEY_SNIPPET = null;

    private View parent_view;
    public LinearLayout linearShare;
    public LinearLayout linearactions;
    public LinearLayout linearDownload;
    public TextView numComments;
    public ImageView imageView;
    PhotoViewAttacher mAttacher;
    private FirebaseAuth mAuth;
    private static final int RC_STORAGE_PERMS = 102;
    private DatabaseReference commentRef;
    public static final String KEY_FILE_URL = "imageProfile";
    Context context;
    Toolbar toolbar;
    String filename;
    File localFile;
    File file;
    public static boolean active = false;
    private static ViewPager mPager;
    private ArrayList<PhotoModel> images;
    private ArrayList<VenteModel> ventes;
    FirebaseStorage storage = FirebaseStorage.getInstance();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_os_full_sreen);
        mAuth = FirebaseAuth.getInstance();
        context = this.getApplicationContext();
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_reload));
        parent_view = findViewById(android.R.id.content);

        images = getIntent().getParcelableArrayListExtra("images");
        ventes = getIntent().getParcelableArrayListExtra("ventes");
        linearShare = findViewById(R.id.linearShare);
        linearactions = findViewById(R.id.linearactions);
        linearDownload = findViewById(R.id.linearDownload);
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new SliderAdapter(getApplicationContext() ,images));
        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(mPager);

        filename = getIntent().getStringExtra("imageProfile");
        if (filename != null){
            Glide.with(imageView.getContext()).load(filename)
//                    .thumbnail(0.5f)
//                    .crossFade()
                    .fitCenter()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(new GlideDrawableImageViewTarget(imageView) {
                        @Override
                        public void onResourceReady(GlideDrawable resource, GlideAnimation<? super GlideDrawable> animation) {
                            super.onResourceReady(resource, animation);
                            //never called
//                        prepareShareIntent();
//                        attachShareIntentAction();
                        }

                        @Override
                        public void onLoadFailed(Exception e, Drawable errorDrawable) {
                            super.onLoadFailed(e, errorDrawable);
                            //never called
                        }
                    });
        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (ActivityMain.mActivity == null){
            Intent intent = new Intent(getApplicationContext(), ActivityMain.class);
            startActivity(intent);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        active = true;
    }

    @Override
    public void onStop() {
        super.onStop();
        active = false;
    }
}
