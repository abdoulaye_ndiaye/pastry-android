package com.recettes.pastry.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.webkit.WebView
import android.webkit.WebViewClient
import com.recettes.pastry.R

/**
 * Created by ghambyte on 22/04/2018 at 21:13.
 * Project name : Tamweel
 */
class ActivityWebView : BaseActivity() {

    @SuppressLint("SetJavaScriptEnabled")
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview_faq)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        val webView: WebView = findViewById(R.id.webView)

        setSupportActionBar(toolbar)
        toolbar.setNavigationIcon(R.drawable.ic_vector_arrow_left)
        toolbar.setNavigationOnClickListener { finish() }
        val webSettings = webView.settings
        webSettings.javaScriptEnabled = true
        webView.webViewClient = WebViewClient()
        webView.loadUrl(getString(R.string.str_privacy))

    }
}