package com.recettes.pastry.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.database.ValueEventListener;
import com.recettes.pastry.R;
import com.recettes.pastry.model.News;
import com.recettes.pastry.viewholder.PostViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;

public class ActivityFavori extends BaseActivity {

    private static final String TAG = "ActivityFavori";
    private FirebaseRecyclerAdapter<News, PostViewHolder> mAdapter;
    private RecyclerView mRecycler;
    InterstitialAd mInterstitialAd;
    private ProgressDialog mProgressDialog;
    private FirebaseAuth mAuth;
    private ProgressBar progressbar;
    TextView text_user_unconnected;
    private Toolbar toolbar;
    private ActionBar actionBar;
    private AdView mAdView;
    LinearLayout lyt_notfound;
    TextView txt_notfound;
    DatabaseReference favRef;


    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Favorites");
        mAdView = findViewById(R.id.adView);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        text_user_unconnected = (TextView) findViewById(R.id.text_user_unconnected);
        lyt_notfound = findViewById(R.id.lyt_notfound);
        txt_notfound = findViewById(R.id.txt_notfound);
        mAuth = FirebaseAuth.getInstance();
        mInterstitialAd = new InterstitialAd(this);
        MobileAds.initialize(this, getString(R.string.str_ad_id));
        mInterstitialAd.setAdUnitId(getString(R.string.str_interstitial));
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });
        requestNewInterstitial();
        mRecycler = findViewById(R.id.recyclerView);
        mRecycler.setHasFixedSize(true);
        text_user_unconnected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getUid() == null){
                    Intent intent = new Intent(getApplicationContext(), ActivityLogin.class);
                    startActivity(intent);
                }
            }
        });

        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        final Query postsQuery = getQuery();
        if (getUid() == null){
            progressbar.setVisibility(View.GONE);
            text_user_unconnected.setVisibility(View.VISIBLE);
            text_user_unconnected.setText(R.string.str_empty_favoris);
        }else if (postsQuery != null){
            initViews();
        }

        favRef = FirebaseDatabase.getInstance().getReference().child("favorit").child(getUid()).child("posts");

        favRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() == 0){
                    lyt_notfound.setVisibility(View.VISIBLE);
                    txt_notfound.setText("No recipes added to your favorites");
                }else {
                    lyt_notfound.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }



    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("SEE_YOUR_LOGCAT_TO_GET_YOUR_DEVICE_ID")
                .build();
        mInterstitialAd.loadAd(adRequest);
    }

    private void onStarClicked(final String id) {
        final DatabaseReference favoriteRef = FirebaseDatabase.getInstance().getReference("favorit").child(getUid()).child(id);
        favoriteRef.removeValue();
        final DatabaseReference postRef = FirebaseDatabase.getInstance().getReference("models").child(id);
        postRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                final News p = mutableData.getValue(News.class);
                if (p == null) {
                    return Transaction.success(mutableData);
                }
                if (p.stars.containsKey(getUid())) {
                    p.starCount = p.starCount - 1;
                    p.stars.remove(getUid());
                }
                mutableData.setValue(p);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b,
                                   DataSnapshot dataSnapshot) {
                Log.d(TAG, "postTransaction:onComplete:" + databaseError);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAdapter != null) {
            mAdapter.cleanup();
        }
    }

    public Query getQuery() {
        if (getUid() != null){
            Query myTopPostsQuery = FirebaseDatabase.getInstance().getReference().child("favorit").child(getUid()).child("posts");
            return myTopPostsQuery;
        }else {
            return null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.str_interstitial));
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });
        requestNewInterstitial();
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage(getString(R.string.str_loading));
        }
        mProgressDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    public void initViews(){
        final Query postsQuery = getQuery();
        mAdapter = new FirebaseRecyclerAdapter<News, PostViewHolder>(News.class, R.layout.row_news, PostViewHolder.class, postsQuery) {

            @Override
            protected void populateViewHolder(final PostViewHolder viewHolder,
                                              final News model,
                                              final int position) {
                if (position % 3 == 0){
                    viewHolder.aditem.setVisibility(View.VISIBLE);
                }else {
                    viewHolder.aditem.setVisibility(View.GONE);
                }
                if (position == 0){
                    viewHolder.aditem.setVisibility(View.GONE);
                }
                viewHolder.bindToPost(model, new View.OnClickListener() {
                    @Override
                    public void onClick(View starView) {

                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View starView) {

                    }
                }, new View.OnClickListener() {
                    @Override
                    public void onClick(View starView) {
                        onStarClicked(model.getId());
                    }
                }, new View.OnClickListener(){
                    @Override
                    public void onClick(View starView) {

                    }
                });
            }
        };
        mRecycler.setHasFixedSize(true);
        mRecycler.setLayoutManager(new LinearLayoutManager(this));
        mRecycler.setAdapter(mAdapter);
        progressbar.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
