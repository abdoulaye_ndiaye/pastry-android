package com.recettes.pastry.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Query;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.recettes.pastry.R;
import com.recettes.pastry.adapter.AdapterIngredientList;
import com.recettes.pastry.adapter.AdapterShopList;
import com.recettes.pastry.controller.App;
import com.recettes.pastry.model.Ingredien;
import com.recettes.pastry.model.News;
import com.recettes.pastry.model.Shop;
import com.recettes.pastry.viewholder.PostViewHolder;
import com.recettes.pastry.viewholder.ShopingListViewHolder;

import java.util.ArrayList;
import java.util.List;

import static com.facebook.FacebookSdk.getApplicationContext;

public class ActivityShopList extends BaseActivity {

    private static final String TAG = "ActivityShopList";
    private FirebaseRecyclerAdapter<Ingredien, ShopingListViewHolder> mAdapter;
    private RecyclerView mRecycler;
    InterstitialAd mInterstitialAd;
    private ProgressDialog mProgressDialog;
    private FirebaseAuth mAuth;
    private ProgressBar progressbar;
    TextView text_user_unconnected;
    private Toolbar toolbar;
    private ActionBar actionBar;
    private AdView mAdView;
    DatabaseReference postRef;
    public static ShopingListAdapter adapter;
    private LinearLayoutManager mManager;
    int positionToDelete;
    LinearLayout lyt_notfound;
    TextView txt_notfound;
    Context context;

    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shoping_list);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Shoping list");
        context = this;
        mAdView = findViewById(R.id.adView);
        progressbar = (ProgressBar) findViewById(R.id.progressbar);
        lyt_notfound = findViewById(R.id.lyt_notfound);
        txt_notfound = findViewById(R.id.txt_notfound);
        text_user_unconnected = (TextView) findViewById(R.id.text_user_unconnected);
        mAuth = FirebaseAuth.getInstance();
        mInterstitialAd = new InterstitialAd(this);
        MobileAds.initialize(this, getString(R.string.str_ad_id));
        mInterstitialAd.setAdUnitId(getString(R.string.str_interstitial));
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });
        requestNewInterstitial();
        mRecycler = findViewById(R.id.recyclerView);
        mRecycler.setHasFixedSize(true);
        text_user_unconnected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getUid() == null){
                    Intent intent = new Intent(getApplicationContext(), ActivityLogin.class);
                    startActivity(intent);
                }
            }
        });

        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        final Query postsQuery = getQuery();
        if (getUid() == null){
            progressbar.setVisibility(View.GONE);
            text_user_unconnected.setVisibility(View.VISIBLE);
            text_user_unconnected.setText(R.string.str_empty_favoris);
        }else {
            initViews();
            postRef = FirebaseDatabase.getInstance().getReference().child("shop").child(getUid());
            adapter = new ShopingListAdapter(getApplicationContext(), postRef);
            mRecycler.setAdapter(adapter);
        }

        postRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getChildrenCount() == 0){
                    lyt_notfound.setVisibility(View.VISIBLE);
                    txt_notfound.setText("Empty Shoping list");
                }else {
                    lyt_notfound.setVisibility(View.INVISIBLE);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("SEE_YOUR_LOGCAT_TO_GET_YOUR_DEVICE_ID")
                .build();
        mInterstitialAd.loadAd(adRequest);
    }

    private void onStarClicked(final String id) {
        final DatabaseReference favoriteRef = FirebaseDatabase.getInstance().getReference("favorit").child(getUid()).child(id);
        favoriteRef.removeValue();
        final DatabaseReference postRef = FirebaseDatabase.getInstance().getReference("models").child(id);
        postRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                final News p = mutableData.getValue(News.class);
                if (p == null) {
                    return Transaction.success(mutableData);
                }
                if (p.stars.containsKey(getUid())) {
                    p.starCount = p.starCount - 1;
                    p.stars.remove(getUid());
                }
                mutableData.setValue(p);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b,
                                   DataSnapshot dataSnapshot) {
                Log.d(TAG, "postTransaction:onComplete:" + databaseError);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mAdapter != null) {
            mAdapter.cleanup();
        }
    }

    public Query getQuery() {
        if (getUid() != null){
            Query myTopPostsQuery = FirebaseDatabase.getInstance().getReference().child("shop").child(getUid());
            return myTopPostsQuery;
        }else {
            return null;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId(getString(R.string.str_interstitial));
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
            }
        });
        requestNewInterstitial();
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage(getString(R.string.str_loading));
        }
        mProgressDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
            case R.id.action_shop_list:
                if (getUid() != null){
                    confirmDialog();
                }
                return true;
        }
        return true;
    }

    public void initViews(){
        final Query postsQuery = getQuery();
//        mAdapter = new FirebaseRecyclerAdapter<Ingredien, ShopingListViewHolder>(Ingredien.class, R.layout.row_news, ShopingListViewHolder.class, postsQuery) {
//
//            @Override
//            protected void populateViewHolder(final ShopingListViewHolder viewHolder,
//                                              final Ingredien model,
//                                              final int position) {
//                viewHolder.bindToPost(model, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View starView) {
//
//                    }
//                }, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View starView) {
//
//                    }
//                }, new View.OnClickListener() {
//                    @Override
//                    public void onClick(View starView) {
//                        onStarClicked(model.getId());
//                    }
//                }, new View.OnClickListener(){
//                    @Override
//                    public void onClick(View starView) {
//
//                    }
//                });
//            }
//        };
        mManager = new LinearLayoutManager(getApplicationContext());
        mManager.setReverseLayout(true);
        mManager.setStackFromEnd(true);
        mRecycler.setLayoutManager(mManager);
        progressbar.setVisibility(View.GONE);
    }

    private class ShopingListAdapter extends RecyclerView.Adapter<ShopingListViewHolder>{

        private Context mContext;
        private DatabaseReference mDatabaseReference;
        private ChildEventListener mChildEventListener;
        private List<String> mCommentIds = new ArrayList<>();
        private List<Shop> mShopList = new ArrayList<>();
        private List<Ingredien> mIngrediens = new ArrayList<>();
        AdapterShopList adapterIngredientList;
        private RecyclerView recyclerIngredients;
        private News news;

        public ShopingListAdapter(final Context context, DatabaseReference ref) {
            mContext = context;
            mDatabaseReference = ref;

            ChildEventListener childEventListener = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {

                    if ((dataSnapshot.getKey()).equals("infos")){
                        news = dataSnapshot.getValue(News.class);
                    }else {
                        mIngrediens = new ArrayList<>();
                        for (DataSnapshot child: dataSnapshot.getChildren()) {
                            if (child.getKey().equals("infos")){
                                news = child.getValue(News.class);
                            }else {
                                mIngrediens.add(child.getValue(Ingredien.class));
                            }
                        }
                        if (mIngrediens.size() != 0){
                            mShopList.add(new Shop(news, mIngrediens));
                            lyt_notfound.setVisibility(View.GONE);
                        }else {
                            lyt_notfound.setVisibility(View.VISIBLE);
                            txt_notfound.setText("Empty Shoping list");
                        }
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    if(positionToDelete >= mShopList.size()){
                        //index not exists
                    }else{
                        mShopList.remove(positionToDelete);
                        notifyItemRemoved(positionToDelete);
                    }
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                }
            };
            ref.addChildEventListener(childEventListener);
            mChildEventListener = childEventListener;
        }

        @Override
        public ShopingListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            View view = inflater.inflate(R.layout.row_shop_item, parent, false);
            return new ShopingListViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ShopingListViewHolder holder, final int position) {
//            final Ingredien ingredien = shop.getList().get(position);
            final Shop shop = mShopList.get(position);
            holder.id_instruction.setText(shop.getInfos().getPosttitre());
            holder.id_instruction.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ActivityNewsDetails.navigate((ActivityShopList)context, holder.id_instruction, shop.getInfos());
                }
            });
            adapterIngredientList =  new AdapterShopList(holder.id_instruction.getContext(), shop.getList());
            holder.recyclerIngredients.setHasFixedSize(true);
            holder.recyclerIngredients.setLayoutManager(new LinearLayoutManager(holder.id_instruction.getContext()));
            holder.recyclerIngredients.setAdapter(adapterIngredientList);
            adapterIngredientList.notifyDataSetChanged();
            holder.del_list.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    postRef.child(shop.getInfos().getId()).removeValue();
                    positionToDelete = holder.getAdapterPosition();

//                    holder.id_instruction.setVisibility(View.GONE);
//                    holder.recyclerIngredients.setVisibility(View.GONE);
                }
            });
//            postRef.child(shop.getInfos().getId()).addListenerForSingleValueEvent(new ValueEventListener() {
//                @Override
//                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                    Log.e("lockeeer", dataSnapshot.getChildrenCount()+"");
//                    if (dataSnapshot.getChildrenCount() == 1){
//                        postRef.child(shop.getInfos().getId()).removeValue();
//                    }
//                }
//
//                @Override
//                public void onCancelled(@NonNull DatabaseError databaseError) {
//
//                }
//            });

//            postRef.child(shop.getInfos().getId()).addChildEventListener((new ChildEventListener() { //attach listener
//
//                @Override
//                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//
//                }
//
//                @Override
//                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//
//                }
//
//                @Override
//                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
//                    Log.e("lockeeer", dataSnapshot.getChildrenCount()+"");
//                    if (dataSnapshot.getChildrenCount() == 13){
//                        postRef.child(shop.getInfos().getId()).removeValue();
//                    }
//                }
//
//                @Override
//                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//
//                }
//
//                @Override
//                public void onCancelled(DatabaseError databaseError) { //update UI here if error occurred.
//
//                }
//            }));
//            holder.id_instruction.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    App.nbrClicks++;
//                    if (mInterstitialAd.isLoaded() && App.nbrClicks > 3) {
//                        showProgressDialog();
//                        mInterstitialAd.show();
//                        mInterstitialAd.setAdListener(new AdListener() {
//                            @Override
//                            public void onAdClosed() {
//                                super.onAdClosed();
////                                App.nbrClicks = 0;
////                                hideProgressDialog();
////                                Intent intent = new Intent(holder.id_instruction.getContext(), ActivityNewsDetails.class);
////                                intent.putExtra(ActivityNewsDetails.EXTRA_OBJC, shop.getInfos());
////                                holder.id_instruction.getContext().startActivity(intent);
//                            }
//                        });
//
//                    } else {
////                        Intent intent = new Intent(id_instruction.getContext(), ActivityNewsDetails.class);
////                        intent.putExtra(ActivityNewsDetails.EXTRA_OBJC, shop.getInfos());
////                        id_instruction.getContext().startActivity(intent);
//                    }
//                }
//            });

//            holder.removeItem.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    final DatabaseReference shopRef = FirebaseDatabase.getInstance().getReference("shop").child(getUid());
////                    shopRef.child(shop.getInfos().getId()).removeValue();
//                }
//            });




//            holder.bindToPost(notificationModel, new View.OnClickListener() {
//                @Override
//                public void onClick(View starView) {
//                    Intent intent = new Intent(getApplicationContext(), ActivityChatDetails.class);
//                    intent.putExtra(EXTRA_POST_KEY, notificationModel.getId());
//                    intent.putExtra("author", notificationModel.getAuthorid()); //UID de celui qui a posté la photo
//                    intent.putExtra("url", notificationModel.getUrl());
//                    startActivity(intent);
//                }
//            });
        }

        @Override
        public int getItemCount() {
            return mShopList.size();
        }

        public void cleanupListener() {
            if (mChildEventListener != null) {
                mDatabaseReference.removeEventListener(mChildEventListener);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_shop, menu);
        return true;
    }

    private void confirmDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Delete shopping list?")
                .setPositiveButton("Oui",  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        final DatabaseReference shopRef = FirebaseDatabase.getInstance().getReference("shop").child(getUid());
                        shopRef.removeValue();
                    }
                })
                .setNegativeButton("Non", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .show();
    }

}
