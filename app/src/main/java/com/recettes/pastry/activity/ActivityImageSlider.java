package com.recettes.pastry.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.recettes.pastry.R;
import com.recettes.pastry.adapter.AndroidImageAdapter;

/**
 * Created by ghambyte on 07/12/2017 at 22:05.
 * Project name : BGFIMobile
 */

public class ActivityImageSlider extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.android_image_slider_activity);

        ViewPager mViewPager = (ViewPager) findViewById(R.id.viewPageAndroid);
        AndroidImageAdapter adapterView = new AndroidImageAdapter(this);
        mViewPager.setAdapter(adapterView);
    }
}
