package com.recettes.pastry.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.google.firebase.auth.FirebaseAuth;
import com.recettes.pastry.R;

public class ActivityParametres extends BaseActivity {
    LinearLayout textViewShare;
    LinearLayout textViewNote;
    LinearLayout linearDisconnect;
    LinearLayout linearPrivacy;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Settings");
        textViewShare = findViewById(R.id.shareApp);
        textViewNote = findViewById(R.id.linearNote);
        linearDisconnect = findViewById(R.id.linearDisconnect);
        linearPrivacy = findViewById(R.id.privacy);
        textViewShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_SUBJECT,
                        getString(R.string.str_share_app_text));
                intent.putExtra(Intent.EXTRA_TEXT,
                        "https://play.google.com/store/apps/details?id=com.recettes.pastry");
                String title = getResources().getString(R.string.share_via);
                Intent chooser = Intent.createChooser(intent, title);

                if (chooser.resolveActivity(getApplicationContext().getPackageManager())
                        != null) {
                    startActivity(chooser);
                }
            }
        });
        textViewNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.recettes.pastry")));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=")));
                }
            }
        });
        linearPrivacy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ActivityWebView.class);
                startActivity(intent);
            }
        });
//        if (getUid() != null){
//            linearDisconnect.setVisibility(View.VISIBLE);
//        }
//        linearDisconnect.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                FirebaseAuth.getInstance().signOut();
//                Intent i = getApplicationContext().getPackageManager()
//                        .getLaunchIntentForPackage( getApplicationContext().getPackageName() );
//                assert i != null;
//                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                startActivity(i);
//            }
//        });
    }

    public static String getUid() {
        if (FirebaseAuth.getInstance().getCurrentUser() == null){
            return null;
        }else {
            return FirebaseAuth.getInstance().getCurrentUser().getUid();

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
