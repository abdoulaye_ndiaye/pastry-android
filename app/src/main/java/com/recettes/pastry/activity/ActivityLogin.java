package com.recettes.pastry.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.recettes.pastry.BaseApp;
import com.recettes.pastry.R;
import com.recettes.pastry.model.Client;
import com.recettes.pastry.model.User;
import com.recettes.pastry.tools.Utilities;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ghambyte on 31/05/2018 at 10:29.
 * Project name : Tamweel
 */

public class ActivityLogin extends BaseActivity {

    private Toolbar toolbar;
    private ActionBar actionBar;
    private AdView mAdView;
    InterstitialAd mInterstitialAd;
    public static Activity mActivity;
    private NavigationView navigationView;
    private final Handler drawerHandler = new Handler();
    TextView mTextViewTitle;
    private static final int RC_TAKE_PICTURE = 101;
    private static final int RC_STORAGE_PERMS = 102;
    private static int RESULT_LOAD_IMAGE = 1;
    private static final String KEY_FILE_URI = "key_file_uri";
    private static final String KEY_DOWNLOAD_URL = "key_download_url";
    public static final String EXTRA_DOWNLOAD_URL = "extra_download_url";
    private static final String TAG = "ActivityMain";
    private Uri mFileUri = null;
    private FloatingActionButton fab,fab1,fab2;
    private Animation fab_open,fab_close,rotate_forward,rotate_backward;
    private Boolean isFabOpen = false;
    private GoogleApiClient mGoogleApiClient;
    private LinearLayout signInGoogleButton;
    private ImageView mPasserField;
    LoginButton loginButtonFacebook;
    EditText mPasswordField;
    EditText mEmailField;
    Button mSignInButton;
    Button mSignUpButton;
    private FirebaseAuth.AuthStateListener mAuthListener;
    String thisUserId;
    public static CallbackManager callbackManagerFacebook;
    public static CallbackManager callbackManager;
    private FirebaseAuth mAuth;
    private static final int RC_SIGN_IN = 9001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initToolbar();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mAuth = FirebaseAuth.getInstance();
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(this.getResources().getString(R.string.default_web_client_id))
                .requestEmail().build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        loginButtonFacebook = findViewById(R.id.button_facebook_login);
        mEmailField = findViewById(R.id.field_email);
        mPasswordField = findViewById(R.id.field_password);
        mPasserField = findViewById(R.id.closeLogin);
        mSignInButton = findViewById(R.id.button_sign_in);
        mSignUpButton = findViewById(R.id.button_sign_up);
        signInGoogleButton = findViewById(R.id.btn_compte_google);
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    if (user.getDisplayName() != null){
                        thisUserId = user.getUid();
                        onAuthSuccessKnownUser(user);
                    }
                } else {
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
            }
        };

        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signInWithPassword();
            }
        });

        mSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signUpWithPassword();
            }
        });

        callbackManagerFacebook = CallbackManager.Factory.create();
        loginButtonFacebook.setReadPermissions("email", "public_profile");
        loginButtonFacebook.registerCallback(callbackManagerFacebook, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                signInWithFacebook(loginResult.getAccessToken());
            }

            @Override

            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
            }
        });

        signInGoogleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signInGoogle();
            }
        });

    }

    private void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        toolbar.setNavigationIcon(R.drawable.ic_vector_close);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult:" + requestCode + ":" + resultCode + ":" + data);
        if (requestCode == RC_TAKE_PICTURE) {
            if (resultCode == RESULT_OK) {
                if (mFileUri != null) {
                    Intent intent = new Intent(getApplicationContext(), NewPostActivity.class)
                            .putExtra("camera_photo", mFileUri.toString());
                    this.startActivity(intent);
                } else {
                    Log.w(TAG, "File URI is null");
                }
            } else {
            }
        }

        if(resultCode != RESULT_CANCELED){
            if (requestCode == RESULT_LOAD_IMAGE) {
                Uri imageUri = CropImage.getPickImageResultUri(this, data);
                try {
                    uploadFromUri(imageUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                hideProgressDialog();
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
                DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                Date date = new Date();
                Log.d("SIGN IN GOOGLE OK :", account.getDisplayName());
//                Client utilisateurC = new Client(account.getDisplayName(), account.getEmail(), dateFormat.format(date), Utilities.getUserCountry(getApplicationContext()));
//                saveClient(utilisateurC);
                Toast.makeText(getApplicationContext(), "success connexion.",
                        Toast.LENGTH_LONG).show();
            } else {
                hideProgressDialog();
                Log.d("SIGN IN GOOGLE OK :", "FAILED");
            }
        }
        if (callbackManagerFacebook!= null){
            callbackManagerFacebook.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void uploadFromUri(Uri fileUri) throws IOException {
        Log.d(TAG, "uploadFromUri:src:" + fileUri.toString());
        Intent intent = new Intent(getApplicationContext(), NewPostActivity.class)
                .putExtra(EXTRA_DOWNLOAD_URL, fileUri.toString());
        startActivity(intent);
    }

    public void signInWithPassword(){
        Log.d(TAG, "signUp");
        if (!validateForm()) {
            return;
        }
        showProgressDialog();
        String email = mEmailField.getText().toString();
        String password = mPasswordField.getText().toString();

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            hideProgressDialog();
                            restartApp();
//                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(ActivityLogin.this, "Failed connexion.",
                                    Toast.LENGTH_SHORT).show();
                            hideProgressDialog();
//                            updateUI(null);
                        }

                        // ...
                    }
                });
    }

    public void signUpWithPassword(){
        Log.d(TAG, "signUp");
        if (!validateForm()) {
            return;
        }
        showProgressDialog();
        String email = mEmailField.getText().toString();
        String password = mPasswordField.getText().toString();
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
//                            updateUI(user);
                            hideProgressDialog();
                            restartApp();
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(ActivityLogin.this, "Failed registration.",
                                    Toast.LENGTH_SHORT).show();
                            hideProgressDialog();
//                            updateUI(null);
                        }

                        // ...
                    }
                });
    }

    private void firebaseAuthWithGoogle(final GoogleSignInAccount acct) {
        showProgressDialog();
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        restartApp();
                        BaseApp.authenticatedUser = new User(acct.getDisplayName(), acct.getEmail(), acct.getFamilyName(), acct.getServerAuthCode());
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
                        final User userG = new User(acct.getDisplayName().toLowerCase(), acct.getEmail(), acct.getPhotoUrl().toString(), FirebaseInstanceId.getInstance().getToken());
                        if (thisUserId != null){
                            Toast.makeText(getApplicationContext(), "Connected!",
                                    Toast.LENGTH_SHORT).show();
                            BaseApp.database.getReference("users").child(thisUserId).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    User user1 = dataSnapshot.getValue(User.class);
                                    if (user1 == null){
                                        BaseApp.database.getReference("users").child(thisUserId).setValue(userG);
                                    }
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                        }
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(getApplicationContext(), "Failed connexion.",
                                    Toast.LENGTH_SHORT).show();
                        }
                        hideProgressDialog();
                    }
                });
    }

    private void signInGoogle() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        (this).startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void signInWithFacebook(AccessToken token) {
        Log.d(TAG, "signInWithFacebook:" + token);
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
                        if (!task.isSuccessful()) {
                            hideProgressDialog();
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(getApplicationContext(), "Failed connexion.",
                                    Toast.LENGTH_SHORT).show();
                        }else{
                            hideProgressDialog();
                            Toast.makeText(getApplicationContext(), "success connexion.",
                                    Toast.LENGTH_LONG).show();
                            String uid=task.getResult().getUser().getUid();
                            String name=task.getResult().getUser().getDisplayName();
                            String email=task.getResult().getUser().getEmail();
                            String image=task.getResult().getUser().getPhotoUrl().toString();
                            final User userF = new User(name.toLowerCase(), email, image, FirebaseInstanceId.getInstance().getToken());
                            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                            Date date = new Date();
                            Client utilisateurC = new Client(name.toLowerCase(), email, dateFormat.format(date), Utilities.getUserCountry(getApplicationContext()));
//                            saveClient(utilisateurC);
                            BaseApp.authenticatedUser = userF;
                            if (getUid() != null){
                                FirebaseDatabase.getInstance().getReference("users").child(getUid()).addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        User user1 = dataSnapshot.getValue(User.class);
                                        if (user1 == null){
                                            FirebaseDatabase.getInstance().getReference("users").child(getUid()).setValue(userF);
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                            }
                        }
                        hideProgressDialog();
                        restartApp();
                    }
                });
    }

    private boolean validateForm() {
        boolean result = true;
        if (TextUtils.isEmpty(mEmailField.getText().toString())) {
            mEmailField.setError("Fournir une adresse valide");
            result = false;
        } else {
            mEmailField.setError(null);
        }

        if (TextUtils.isEmpty(mPasswordField.getText().toString())) {
            mPasswordField.setError("Fournir un mot de passe valide");
            result = false;
        } else {
            mPasswordField.setError(null);
        }

        return result;
    }

    private void onAuthSuccessKnownUser(FirebaseUser user) {

    }

    public void restartApp(){
        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage( getBaseContext().getPackageName() );
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }
}
