package com.recettes.pastry.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.recettes.pastry.R;
import com.recettes.pastry.service.MyUploadService;
import com.recettes.pastry.service.ProfileUploadService;

import static com.recettes.pastry.service.MyUploadService.EXTRA_FILE_URI;
import static com.recettes.pastry.service.MyUploadService.EXTRA_FILE_URI1;
import static com.recettes.pastry.service.MyUploadService.EXTRA_FILE_URI2;
import static com.recettes.pastry.service.MyUploadService.EXTRA_FILE_URI3;
import static com.recettes.pastry.service.MyUploadService.EXTRA_FILE_URI4;
import static com.recettes.pastry.service.MyUploadService.EXTRA_FILE_URI5;

/**
 * Created by ghambyte on 29/05/2018 at 11:13.
 * Project name : Tamweel
 */

public class ActivityLaRecette extends BaseActivity {

    ListView listView;
    ArrayAdapter<String> adapter;
    Uri param0 = null;
    Uri param1 = null;
    Uri param2 = null;
    Uri param3 = null;
    Uri param4 = null;
    Uri param5 = null;
    String ingredients;
    String category;
    private Button mPostButton;
    private EditText mEditRecette;
    private EditText mEditTitre;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_larecette);
        Toolbar mToolBar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolBar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        mToolBar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mPostButton = findViewById(R.id.id_post_recette_button);
        mEditRecette = findViewById(R.id.id_recette_text);
        mEditTitre = findViewById(R.id.id_recette_titre);
        Intent intent = getIntent();
        param0 = intent.getParcelableExtra(EXTRA_FILE_URI);
        param1 = intent.getParcelableExtra(EXTRA_FILE_URI1);
        param2 = intent.getParcelableExtra(EXTRA_FILE_URI2);
        param3 = intent.getParcelableExtra(EXTRA_FILE_URI3);
        param4 = intent.getParcelableExtra(EXTRA_FILE_URI4);
        param5 = intent.getParcelableExtra(EXTRA_FILE_URI5);
        ingredients = intent.getStringExtra("ingredients");
        category = intent.getStringExtra("category");
        Log.e("Ingredients #######", ingredients);

        mPostButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitPost();
            }
        });

        mEditRecette.addTextChangedListener(contentWatcher);
        if (mEditRecette.length() == 0) {
            mPostButton.setEnabled(false);
        }
    }

    private void submitPost() {
        Toast.makeText(this, "Ajoût de la photo...", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getApplicationContext(), ActivityMain.class);
        startService(new Intent(getApplicationContext(), MyUploadService.class)
                .putExtra(EXTRA_FILE_URI, param0)
                .putExtra(EXTRA_FILE_URI1, param1)
                .putExtra(EXTRA_FILE_URI2, param2)
                .putExtra(EXTRA_FILE_URI3, param3)
                .putExtra(EXTRA_FILE_URI4, param4)
                .putExtra(EXTRA_FILE_URI5, param5)
                .putExtra("category", category)
                .putExtra("ingredients", ingredients)
                .putExtra("recette", mEditRecette.getText().toString())
                .putExtra("titre", mEditTitre.getText().toString())
                .setAction(ProfileUploadService.ACTION_UPLOAD));
        this.startActivity(intent);
        finish();
    }

    private TextWatcher contentWatcher = new TextWatcher() {
        @Override
        public void afterTextChanged(Editable etd) {
            if (etd.toString().trim().length() == 0) {
                mPostButton.setEnabled(false);
            } else {
                mPostButton.setEnabled(true);
            }
            //draft.setContent(etd.toString());
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        }
    };
}
