package com.recettes.pastry.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.recettes.pastry.R;
import com.recettes.pastry.adapter.PageFragmentAdapter;

public class FragmentLesRecettesdeBase extends Fragment {


    String category = "les recettes de base";

    private PageFragmentAdapter adapter;
    private ViewPager viewPager;
    private TabLayout tabLayout;

    private FragmentCategoriesRecettesDeBase f_categories;
    private FragmentLesBases f_inspi;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View result=inflater.inflate(R.layout.fragment_patisserie, container, false);
        viewPager= result.findViewById(R.id.viewpager);

        viewPager.setAdapter(buildAdapter());
        tabLayout = result.findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        setupTabIcons();
        setupTabClick();

        return(result);
    }

    private PagerAdapter buildAdapter() {
        adapter = new PageFragmentAdapter(getChildFragmentManager());
        if (f_categories == null) {
            f_categories = new FragmentCategoriesRecettesDeBase();
        }
        if (f_inspi == null) {
            f_inspi = new FragmentLesBases();
        }
        adapter.addFragment(f_categories, "CATEGORIES");
        adapter.addFragment(f_inspi, "BASIC RECIPES");
        viewPager.setAdapter(adapter);
        return(adapter);
    }

    private void setupTabClick() {
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int position = tab.getPosition();
                viewPager.setCurrentItem(position);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
    }

    private void setupTabIcons() {
        tabLayout.getTabAt(0).setText("CATEGORIES");
        tabLayout.getTabAt(1).setText("BASIC RECIPES");
    }
}
