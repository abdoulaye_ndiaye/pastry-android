package com.recettes.pastry.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.recettes.pastry.activity.ActivityMain;
import com.recettes.pastry.activity.ActivityNewsDetails;
import com.recettes.pastry.BaseApp;
import com.recettes.pastry.R;
import com.recettes.pastry.adapter.RecipeGridAdapter;
import com.recettes.pastry.controller.App;
import com.recettes.pastry.data.Tools;
import com.recettes.pastry.model.News;
import com.recettes.pastry.model.PostResp;
import com.recettes.pastry.rest.PostService;
import com.recettes.pastry.tools.Utilities;
import com.recettes.pastry.view.EndlessRecyclerOnScrollListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

public abstract class FragmentHome extends Fragment {

    private View root_view;
    private RecyclerView recyclerView;
    private ProgressBar progressbar;
    List<News> postItemList;
    TextView text_empry_list;
    ImageButton tryAgain;
    private int nextPage = 1;
    PostService postService = BaseApp.mobileAdapter.create(PostService.class);
    public static RecipeGridAdapter mAdapter;
    SwipeRefreshLayout swipeRefreshLayout;
    private SearchView searchView;
    private boolean hasPage = true;
    InterstitialAd mInterstitialAd;
    Context context;
    public static ProgressDialog mProgressDialog;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        root_view = inflater.inflate(R.layout.fragment_home, null);
        context = getActivity();
        setHasOptionsMenu(true);

        recyclerView = root_view.findViewById(R.id.recyclerView);
        progressbar = root_view.findViewById(R.id.progressbar);
        text_empry_list = root_view.findViewById(R.id.text_empry_list);
        tryAgain = root_view.findViewById(R.id.try_again);
        swipeRefreshLayout = root_view.findViewById(R.id.swipeRefreshLayout);
        initTabLayout();
        setRetainInstance(true);
        mInterstitialAd = new InterstitialAd(getApplicationContext());
        mInterstitialAd.setAdUnitId(getString(R.string.str_interstitial));
        requestNewInterstitial();
        tryAgain.setOnClickListener(tryAgainListener);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                nextPage = 1;
                getclientInfos(getCategory());
//                if (mInterstitialAd.isLoaded()){
////                    showProgressDialog();
//                    mInterstitialAd.show();
//                    mInterstitialAd.setAdListener(new AdListener() {
//                        @Override
//                        public void onAdClosed() {
//                            super.onAdClosed();
////                            hideProgressDialog();
//                            getclientInfos(getCategory());
//                        }
//                    });
//                }else {
//                    getclientInfos(getCategory());
//                }
            }
        });

        return root_view;
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("SEE_YOUR_LOGCAT_TO_GET_YOUR_DEVICE_ID")
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

    private View.OnClickListener tryAgainListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            progressbar.setVisibility(View.VISIBLE);
            tryAgain.setVisibility(View.GONE);
            nextPage = 1;
            getclientInfos(getCategory());
        }
    };

    private void getclientInfos(String category) {
        if (!Utilities.isInternetAvailable(getActivity())) {
            progressbar.setVisibility(View.GONE);
            Toast.makeText(getApplicationContext(), getString(R.string.no_internet), Toast.LENGTH_LONG).show();
            tryAgain.setVisibility(View.VISIBLE);
            return;
        }
        progressbar.setVisibility(View.VISIBLE);
        Call<PostResp> operations;
        operations = postService.postlist("", category, "", "", nextPage);
        operations.enqueue(callback);
    }

    private Callback<PostResp> page = new Callback<PostResp>() {
        @Override
        public void onResponse(Call<PostResp> call, Response<PostResp> res) {
            progressbar.setVisibility(View.VISIBLE);
            int code = res.code();
            if (code == 200) {
                postItemList.addAll(res.body().getPostItems());
                mAdapter.notifyDataSetChanged();

                progressbar.setVisibility(View.GONE);
            }
            if (code == 401) {
                ResponseBody base = res.errorBody();
                if (base != null) {
                    //TODO Client inconnu. Redirect to login screen
                    Toast.makeText(getActivity(), res.message(), Toast.LENGTH_SHORT)
                            .show();
                }
                getActivity().finish();
            }
            if (code == 500) {
                Toast.makeText(getActivity(), R.string.str_unknown_error, Toast.LENGTH_SHORT)
                        .show();
            }
            progressbar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PostResp> call, Throwable t) {
            progressbar.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            tryAgain.setVisibility(View.VISIBLE);
            Toast.makeText(getActivity(), R.string.str_service_unavailable, Toast.LENGTH_SHORT)
                    .show();
        }
    };

    private Callback<PostResp> callback = new Callback<PostResp>() {

        @Override
        public void onResponse(Call<PostResp> call, Response<PostResp> res) {
            int code = res.code();
            if (code == 200) {
                postItemList = res.body().getPostItems();
                if (postItemList.isEmpty()){
                    text_empry_list.setVisibility(View.VISIBLE);
                }else {
                    text_empry_list.setVisibility(View.GONE);
                }
                initViews();
            } else if (code == 401) {
                ResponseBody base = res.errorBody();
                if (base != null) {
                    Toast.makeText(getActivity(), base.toString(), Toast.LENGTH_SHORT).show();
                }

                getActivity().finish();
            } else if (code == 500) {
                Toast.makeText(getActivity(), R.string.str_unknown_error, Toast.LENGTH_SHORT)
                        .show();
            } else {
                Utilities.showMessage(R.string.str_zactu, R.string.str_service_unavailable, context);
            }
            progressbar.setVisibility(View.GONE);
        }

        @Override
        public void onFailure(Call<PostResp> call, Throwable t) {
            progressbar.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
            tryAgain.setVisibility(View.VISIBLE);
            Toast.makeText(context, R.string.str_service_unavailable, Toast.LENGTH_SHORT)
                    .show();
        }
    };

    private void initTabLayout(){
        getclientInfos(getCategory());
    }

    private void initViews() {
        recyclerView.setVisibility(View.VISIBLE);
        LinearLayoutManager layoutManager = new GridLayoutManager(getApplicationContext(), Tools.calculateNoOfColumns(getApplicationContext()));
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        layoutManager.scrollToPosition(0);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addOnScrollListener(new EndlessRecyclerOnScrollListener(layoutManager) {
            @Override
            public boolean onLoadMore(int current_page) {
                if (!hasPage) {
                    return false;
                }
                if (!Utilities.isInternetAvailable(getActivity())) {
                    Toast.makeText(getActivity(), R.string.str_no_internet, Toast.LENGTH_SHORT)
                            .show();
                    tryAgain.setVisibility(View.VISIBLE);
                    return false;
                }
                progressbar.setVisibility(View.VISIBLE);
                Call<PostResp> operations = postService
                        .postlist("", getCategory(), "", "", current_page + 1);
                operations.enqueue(page);
                return true;
            }
        });

        if (postItemList.size() == 30) {
            nextPage++;
            hasPage = true;
        } else {
            hasPage = false;
        }
        mAdapter = new RecipeGridAdapter(getActivity(), postItemList);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        mAdapter.notifyDataSetChanged();

        mAdapter.setOnItemClickListener(new RecipeGridAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(final View v, final News obj, int position) {
                App.nbrClicks++;
                if (App.nbrClicks > 3 && mInterstitialAd.isLoaded()){
                    showProgressDialog();
                    mInterstitialAd.show();
                    mInterstitialAd.setAdListener(new AdListener() {
                        @Override
                        public void onAdClosed() {
                            super.onAdClosed();
                            App.nbrClicks = 0;
                            hideProgressDialog();
                            ActivityNewsDetails.navigate((ActivityMain)getActivity(), v.findViewById(R.id.image), obj);
                        }
                    });
                }else {
                    ActivityNewsDetails.navigate((ActivityMain)getActivity(), v.findViewById(R.id.image), obj);
                }

            }
        });
        swipeRefreshLayout.setRefreshing(false);
        progressbar.setVisibility(View.GONE);
    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_activity_main, menu);
        final MenuItem searchItem = menu.findItem(R.id.action_search);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setIconified(false);
        searchView.setQueryHint("Search...");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                try {
                    mAdapter.getFilter().filter(s);
                } catch (Exception e) {
                }
                return true;
            }
        });
        // Detect SearchView icon clicks
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setItemsVisibility(menu, searchItem, false);
            }
        });

        // Detect SearchView close
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                setItemsVisibility(menu, searchItem, true);
                return false;
            }
        });
        searchView.onActionViewCollapsed();
    }

    private void setItemsVisibility(Menu menu, MenuItem exception, boolean visible) {
        for (int i = 0; i < menu.size(); ++i) {
            MenuItem item = menu.getItem(i);
            if (item != exception) item.setVisible(visible);
        }
    }

    @Override
    public void onResume() {
        requestNewInterstitial();
        super.onResume();
    }

    @Override
    public void onPause() {
        requestNewInterstitial();
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public abstract String getCategory();

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage(getString(R.string.str_loading));
        }

        mProgressDialog.show();
    }

    public static void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }

}
